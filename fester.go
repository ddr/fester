package main

import (
	"bufio"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/jordan-wright/email"
)

const (
	TargetFileExtension         = ".tif"
	TargetFilePattern           = "*" + TargetFileExtension
	DefaultConfigFile           = ".fester.json"
	FromAddress                 = "Uncle Fester <no-reply@duke.edu>"
	SMTPHost                    = "smtp.duke.edu:25"
	ByteOrderMark               = "\uFEFF"
	ErrorMessage                = "error"
	WarningMessage              = "warning"
	DefaultAttachmentName       = "manifest.csv"
	ManifestMediaType           = "text/csv"
	DefaultStandardChecksumFile = "manifest-sha1.txt"
	DefaultStandardContentFiles = "data"
	DuplicateFileMessageType    = WarningMessage
)

var MessageKinds = []string{ErrorMessage, WarningMessage}

var ErrHeaderEmpty = errors.New("a column header is empty")
var ErrChecksumInvalid = errors.New("checksum is invalid")
var ErrStorageVolumeNotSet = errors.New("storage volume not set")

// option errors
var ErrRootRequired = errors.New("root directory is required")
var ErrItemIdLengthInvalid = errors.New("item id length must be greater than or equal to zero")
var ErrItemIdLengthNotSupported = errors.New("item id length is not supported for this package type")
var ErrPackageTypeRequired = errors.New("-package-type or -manifest-file option is required")
var ErrCollectionPerFolderNotSupported = errors.New("-collection-per-folder is not supported for this package type")

// manifest errors
var ErrModelInvalid = "invalid model: %s"
var ErrModelMissing = "missing model: %s"
var ErrChecksumMissing = "missing checksum: %s"
var ErrChecksumNotUnique = "checksum not unique: %s"
var ErrPathMissing = "missing path: %s"
var ErrPathNotUnique = "path not unique: %s"
var ErrFilePathIsDir = "file path is a directory: %s"
var ErrFileIsEmpty = "file is empty: %s"
var ErrFilePathIsEmpty = "file path is empty: %s"
var ErrCollectionPathNotEmpty = "collection path is not empty: %s"
var ErrCollectionTitleOrIdRequired = "collection title or id is required: %s"
var ErrContentResourcePathOrIdRequired = "content resource path or id is required: %s"
var ErrComponentIdOrParentRequired = "component id or parent item is required: %s"
var ErrFileWithoutResource = "file without a resource: %s"
var ErrIdNotUnique = "id not unique: %s"
var ErrItemPathNotEmpty = "item path is not empty: %s"
var ErrLocalIdNotUnique = "local id not unique: %s"
var ErrManifestMissingModel = fmt.Errorf("manifest file is missing '%s' column", ModelCol)
var ErrContentResourcePathAndIdPresent = "content resource path and id cannot both be present: %s"

type Message struct {
	Text string
	Kind string
}

func (m *Message) IsError() bool {
	return m.Kind == ErrorMessage
}

func (m *Message) IsWarning() bool {
	return m.Kind == WarningMessage
}

type Fester struct {
	*Options
	Manifest      *Table
	Messages      []Message // non-fatal errors and warnings
	ExcludedPaths []string
	Metadata      *Table
	Checksums     *Checksums // maps paths to checksums
	ContentVolume string     // volume containing content files

	config           *Options            // config file options
	commandLine      *CommandLine        // command-line options
	componentFiles   map[string][]string // paths to other component files
	nonContentPaths  []string            // paths to non-content files
	lastItemLocalId  string              // DPC Folder only
	metadataIndex    map[string]int      // map of metadata key value to row index
	lastItemRow      int                 // last item row
	lastComponentRow int                 // last component row
}

func NewFester() *Fester {
	return &Fester{
		Manifest:         &Table{Headers: DefaultManifestHeaders},
		Messages:         make([]Message, 0),
		Metadata:         &Table{},
		Options:          NewOptions(),
		Checksums:        &Checksums{},
		config:           NewOptions(),
		commandLine:      NewCommandLine(),
		nonContentPaths:  make([]string, 0),
		componentFiles:   make(map[string][]string),
		lastItemLocalId:  "",
		lastItemRow:      -1,
		lastComponentRow: -1,
		metadataIndex:    make(map[string]int),
	}
}

func (f *Fester) HasItemIdLength() bool {
	return f.ItemIdLength != 0
}

func (f *Fester) IsGenericFolderPackage() bool {
	return f.PackageType == GenericFolderPackage
}

func (f *Fester) IsDPCFolderPackage() bool {
	return f.PackageType == DPCFolderPackage
}

func (f *Fester) IsStandardPackage() bool {
	return f.PackageType == StandardPackage
}

func (f *Fester) IsNestedFolderPackage() bool {
	return f.PackageType == NestedFolderPackage
}

func (f *Fester) WriteOptions(w io.Writer) error {
	return f.Options.Write(w)
}

func (f *Fester) MergeOptions(o *Options) {
	fields := reflect.VisibleFields(reflect.TypeOf(*o))

	fValue := reflect.ValueOf(f)
	oValue := reflect.ValueOf(o)

	for _, field := range fields {
		fField := fValue.Elem().FieldByName(field.Name)
		oField := oValue.Elem().FieldByName(field.Name)

		if fField.CanSet() && !oField.IsZero() {
			fField.Set(oField)
		}
	}
}

func (f *Fester) LoadConfig(configFile string) {
	f.config.Load(configFile)
}

// Returns the path as rooted in the content volume
func (f *Fester) ContentVolumePath(path string) (volPath string, err error) {
	f.debug("ContentVolumePath: %s\n", path)

	if f.ContentVolume == "" {
		return path, ErrStorageVolumeNotSet
	}

	if path == "" { // empty path is a no-op
		f.debug("Empty path!\n")
		return path, nil
	}

	// N.B. filepath.IsAbs() returns false on non-Windows platforms
	// for Windows-style UNC paths (i.e., starting with `\\`); however,
	// we must take care not to call filepath.Abs() on a UNC path
	// with forward slashes, because it runs filepath.Clean(), which
	// compacts the initial forward slashes into a single slash.
	if !strings.HasPrefix(path, `\\`) && !filepath.IsAbs(path) {
		f.debug("Converting path to absolute: %s\n", path)
		path, err = filepath.Abs(path)
		if err != nil {
			return path, err
		}
		f.debug("Converted absolute path: %s\n", path)
	}

	// Now we have an abolute path, which may be a Windows-style UNC path;
	// If so, we need to convert it to a Unix-style, forward-slash prefixed path.
	if strings.HasPrefix(path, `\\`) {
		f.debug("Converting path to unix-style: %s\n", path)
		path = strings.ReplaceAll(path, "\\", "/")
		f.debug("Converted unix-style path: %s\n", path)
	}

	// path matches content volume?
	// This would be the case for a Unix-style UNC path
	if strings.HasPrefix(path, f.ContentVolume) {
		f.debug("Path matches content volume prefix: %s\n", path)
		return path, nil
	}

	// path matches DDR mount?
	ddrMount := DDRMount(f.ContentVolume)
	if strings.HasPrefix(path, ddrMount) {
		f.debug("Path matches DDR mount prefix: %s\n", path)
		volPath = f.ContentVolume + strings.TrimPrefix(path, ddrMount)
		f.debug("Volume path: %s\n", volPath)
		return volPath, nil
	}

	// path matches Mac mount?
	macMount := MacMount(f.ContentVolume)
	if strings.HasPrefix(path, macMount) {
		f.debug("Path matches Mac mount prefix: %s\n", path)
		volPath = f.ContentVolume + strings.TrimPrefix(path, macMount)
		f.debug("Volume path: %s\n", volPath)
		return volPath, nil
	}

	// UNC paths - we have to worry about case insensitivity
	if strings.HasPrefix(path, "//") {
		f.debug("UNC path: %s\n", path)
		if strings.HasPrefix(strings.ToLower(path), strings.ToLower(f.ContentVolume)) {
			f.debug("Path matches content volume prefix: %s\n", path)
			volPath = f.ContentVolume + path[len(f.ContentVolume):]
			f.debug("Volume path: %s\n", volPath)
			return volPath, nil
		}
	}

	err = errors.New("Unable to find path on volume: " + path)
	return path, err
}

func (f *Fester) SetContentVolume() {
	root := strings.ReplaceAll(f.ContentPath(), "\\", "/")

	for _, volume := range StorageVolumes {
		if strings.HasPrefix(root, volume) ||
			strings.HasPrefix(root, DDRMount(volume)) ||
			strings.HasPrefix(root, MacMount(volume)) {
			f.ContentVolume = volume
			f.debug("Content volume set: %s\n", f.ContentVolume)
			break
		}
	}
}

func (f *Fester) Interactive() bool {
	return !f.NonInteractive
}

func (f *Fester) SetNonContentPaths() {
	for _, path := range []string{
		f.CaptionFiles.String(),
		f.ChecksumFile.String(),
		f.DerivedImageFiles.String(),
		f.ExtractedTextFiles.String(),
		f.IntermediateFiles.String(),
		f.MetadataFile.String(),
		f.MultiresImageFiles.String(),
		f.StreamableMediaFiles.String(),
		f.TargetFiles.String(),
		f.ThumbnailFiles.String(),
	} {
		if path != "" {
			f.nonContentPaths = append(f.nonContentPaths, path)
		}
	}
}

func walkComponentFiles(root string) (paths []string) {
	if root == "" {
		return paths
	}
	filepath.WalkDir(root, func(path string, d os.DirEntry, err error) error {
		if err != nil {
			panic(err)
		}
		if d.IsDir() {
			return nil
		}
		paths = append(paths, path)
		return nil
	})
	return paths
}

func (f *Fester) SetComponentFiles() {
	f.componentFiles[CaptionFileModel] = walkComponentFiles(f.CaptionFiles.String())
	f.componentFiles[DerivedImageFileModel] = walkComponentFiles(f.DerivedImageFiles.String())
	f.componentFiles[ExtractedTextFileModel] = walkComponentFiles(f.ExtractedTextFiles.String())
	f.componentFiles[IntermediateFileModel] = walkComponentFiles(f.IntermediateFiles.String())
	f.componentFiles[MultiresImageFileModel] = walkComponentFiles(f.MultiresImageFiles.String())
	f.componentFiles[StreamableMediaFileModel] = walkComponentFiles(f.StreamableMediaFiles.String())
	f.componentFiles[ThumbnailFileModel] = walkComponentFiles(f.ThumbnailFiles.String())
}

func (f *Fester) IsExcludedPathName(fileName string) bool {
	for _, name := range f.ExcludePathNames {
		matched, err := filepath.Match(name, fileName)
		if err != nil { // bad pattern
			panic(err)
		}
		if matched {
			return true
		}
	}
	return false
}

func (f *Fester) IsExcludedContentPath(d os.DirEntry, path string) bool {
	if f.IsExcludedPathName(d.Name()) {
		return true
	}
	for _, p := range f.ExcludeContentPaths {
		if matched, _ := filepath.Match(p.String(), path); matched {
			return true
		}
	}
	return false
}

func (f *Fester) IsNonContentPath(path string) bool {
	return listContains(f.nonContentPaths, path)
}

// ContentPath returns the absolute path to the content files.
func (f *Fester) ContentPath() string {
	if f.ContentFiles.String() != "" {
		return f.ContentFiles.String()
	}
	return f.Root.String()
}

func (f *Fester) ManifestOptions() string {
	var b strings.Builder
	f.WriteOptions(&b)
	return b.String()
}

func (f *Fester) PrintOptions(w io.Writer) {
	fmt.Fprintln(w, "------------------------------")
	fmt.Fprintln(w, "       Manifest Options")
	fmt.Fprintln(w, "------------------------------")
	f.WriteOptions(w)
	fmt.Fprintln(w, "------------------------------")
}

func (f *Fester) ConfirmOrExit(w io.Writer) {
	fmt.Fprint(w, "\nGenerate manifest (y/N)? ")
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	if err := input.Err(); err != nil {
		log.Fatalln(err)
	}
	if input.Text() != "y" {
		fmt.Println("\nUncle Fester says \"See-ya!\"")
		os.Exit(0)
	}
}

func (f *Fester) Message(msg interface{}, kind string) {
	if msg == nil {
		return
	}
	if kind == "" {
		kind = ErrorMessage
	}
	if !listContains(MessageKinds, kind) {
		panic(fmt.Sprintf("invalid message kind: %s", kind))
	}

	text := fmt.Sprintf("%v", msg)
	if text != "" {
		message := Message{Text: text, Kind: kind}
		f.Messages = append(f.Messages, message)
	}
}

func (f *Fester) Error(msg interface{}) {
	f.Message(msg, ErrorMessage)
}

func (f *Fester) Errorf(format string, a ...interface{}) {
	f.Error(fmt.Sprintf(format, a...))
}

func (f *Fester) Warning(msg interface{}) {
	f.Message(msg, WarningMessage)
}

func (f *Fester) Warningf(format string, a ...interface{}) {
	f.Warning(fmt.Sprintf(format, a...))
}

func (f *Fester) debug(fmt string, a ...interface{}) {
	if f.Debug {
		log.Printf(fmt, a...)
	}
}

func (f *Fester) WriteManifest(w io.Writer) (err error) {
	csvWriter := csv.NewWriter(w)

	// write the header row
	err = csvWriter.Write(f.ManifestHeaders())
	if err != nil {
		return err
	}
	// write the manifest rows
	for _, row := range f.ManifestRows() {
		data := make([]string, len(f.ManifestHeaders()))
		for i, header := range f.ManifestHeaders() {
			value, _ := row.Get(header)
			data[i] = value
		}
		err = csvWriter.Write(data)
		if err != nil {
			return err
		}
	}

	// Flush writes any buffered data to the underlying io.Writer.
	csvWriter.Flush()

	// Check for any errors that occurred during the write process
	return csvWriter.Error()
}

// Adds path to list of excluded paths
func (f *Fester) ExcludePath(path string) {
	f.ExcludedPaths = append(f.ExcludedPaths, path)
}

func (f *Fester) ManifestCollectionCount() int {
	return f.ManifestModelCount(CollectionModel)
}

func (f *Fester) ManifestItemCount() int {
	return f.ManifestModelCount(ItemModel)
}

func (f *Fester) ManifestComponentCount() int {
	return f.ManifestModelCount(ComponentModel)
}

func (f *Fester) ManifestTargetCount() int {
	return f.ManifestModelCount(TargetModel)
}

func (f *Fester) ManifestAttachmentCount() int {
	return f.ManifestModelCount(AttachmentModel)
}

func (f *Fester) ManifestRowsByModel(model string) (rows []Row) {
	for _, row := range f.ManifestRows() {
		if row.Model() == model {
			rows = append(rows, row)
		}
	}
	return rows
}

func (f *Fester) ManifestModelCount(model string) (count int) {
	return len(f.ManifestRowsByModel(model))
}

func (f *Fester) ManifestSummary() string {
	var b strings.Builder
	f.PrintSummary(&b)
	return b.String()
}

func (f *Fester) PrintSummary(w io.Writer) {
	collectionCount := f.ManifestCollectionCount()
	itemCount := f.ManifestItemCount()
	componentCount := f.ManifestComponentCount()
	attachmentCount := f.ManifestAttachmentCount()
	targetCount := f.ManifestTargetCount()
	totalResources := collectionCount + itemCount + componentCount + attachmentCount + targetCount

	fmt.Fprintln(w, "------------------------------")
	fmt.Fprintln(w, "       Manifest Summary")
	fmt.Fprintln(w, "------------------------------")
	fmt.Fprintf(w, "Resources:     %3d\n", totalResources)
	fmt.Fprintf(w, "  Collections: %3d\n", collectionCount)
	fmt.Fprintf(w, "  Items:       %3d\n", itemCount)
	fmt.Fprintf(w, "  Components:  %3d\n", componentCount)
	fmt.Fprintf(w, "  Targets:     %3d\n", targetCount)
	fmt.Fprintf(w, "  Attachments: %3d\n", attachmentCount)

	// files
	fmt.Fprintln(w, "Other Files:")
	for _, model := range FileModels {
		numFiles := f.ManifestModelCount(model)
		fmt.Fprintf(w, "  %s: %d\n", model, numFiles)
	}

	// excluded paths
	if len(f.ExcludedPaths) > 0 {
		fmt.Fprintln(w, "Excluded paths:")
		for _, path := range f.ExcludedPaths {
			fmt.Fprintln(w, "  ", path)
		}
	}

	// messages
	fmt.Fprintf(w, "Messages: %d\n", len(f.Messages))
	for _, e := range f.Messages {
		if e.Kind == WarningMessage && !f.Verbose {
			continue
		}
		log.Println(e)
	}
	fmt.Fprintln(w, "------------------------------")
}

func (f *Fester) EmailManifest() (err error) {
	temp, err := os.CreateTemp("", "manifest-*.csv")
	if err != nil {
		return err
	}
	defer os.Remove(temp.Name())

	err = f.WriteManifest(temp)
	if err != nil {
		return err
	}

	temp.Close() // close for writing

	reader, err := os.Open(temp.Name())
	if err != nil {
		return err
	}
	defer reader.Close()

	message := fmt.Sprintf(`Please find attached the ingest manifest you requested.

Thanks,
Uncle Fester

%s

Fester Command:

%v

Manifest Options:

%s
`, f.ManifestSummary(), os.Args, f.Options)

	mailTo := make([]string, len(f.Mailto))
	for i, address := range f.Mailto {
		mailTo[i] = address.String()
	}

	e := email.NewEmail()
	e.From = FromAddress
	e.To = mailTo
	e.Subject = "Ingest Manifest"
	e.Text = []byte(message)

	attachmentName := DefaultAttachmentName
	if f.OutputFile != "" { // use base name of -output-file option if specified
		attachmentName = filepath.Base(f.OutputFile.String())
	}
	_, err = e.Attach(reader, attachmentName, ManifestMediaType)
	if err != nil {
		return err
	}

	return e.Send(SMTPHost, nil)
}

func (f *Fester) ParseCommandLine() {
	f.commandLine.ParseCommand()
}

func (f *Fester) ValidateOptions() (validationErrors []error) {
	return f.Options.Validate()
}

func main() {
	var err error
	f := NewFester()

	// load config file and command line options
	f.ParseCommandLine()
	f.LoadConfig(f.commandLine.ConfigFile)

	// merge config (first) and command line options (second)
	f.MergeOptions(f.config)
	f.MergeOptions(f.commandLine.Options)

	// if package type is standard, merge standard package options,
	// but don't overwrite any options that were already set
	if f.PackageType.String() == StandardPackage {
		if f.ContentFiles == "" {
			dataDir := filepath.Join(f.Root.String(), DefaultStandardContentFiles)
			if _, err := os.Stat(dataDir); err == nil {
				f.ContentFiles.Set(dataDir)
			} else {
				log.Fatalf("No %s directory found in root directory for %s package\n",
					DefaultStandardContentFiles, StandardPackage)
			}
		}
		if f.ChecksumFile == "" {
			checksumFile := filepath.Join(f.Root.String(), DefaultStandardChecksumFile)
			if _, err := os.Stat(checksumFile); err == nil {
				f.ChecksumFile.Set(checksumFile)
			} else {
				log.Fatalf("No %s file found in root directory for %s package\n",
					DefaultStandardChecksumFile, StandardPackage)
			}
		}
	}

	// Debug implies Verbose
	if f.Debug {
		f.Verbose = true
	}

	// if requested, generate config file and exit ...
	if f.commandLine.GenerateConfig {
		if f.OutputFile != "" { // write to file
			outputFile, err := os.Create(f.OutputFile.String())
			if err != nil { // path error
				log.Fatalln(err)
			}
			defer outputFile.Close()
			err = f.WriteOptions(outputFile)
			if err != nil {
				log.Fatalln(err)
			}
			log.Println("Config file written to", f.OutputFile.String())
		} else { // ... or write to stdout
			err = f.WriteOptions(os.Stdout)
			if err != nil {
				log.Fatalln(err)
			}
		}
		os.Exit(0)
	}
	// ... otherwise, validate options
	validationErrors := f.ValidateOptions()
	if len(validationErrors) > 0 {
		for _, e := range validationErrors {
			log.Println(e)
		}
		log.Fatalln("Exiting due to validation errors")
	}

	// Print options and get confirmation from user, if interactive
	f.PrintOptions(os.Stderr)
	if f.Interactive() {
		f.ConfirmOrExit(os.Stderr)
	}

	// Generate the manifest
	err = f.GenerateManifest()
	if err != nil {
		log.Fatalln(err) // exit on error
	}

	// Print the manifest
	if len(f.Mailto) > 0 { // email the manifest
		err = f.EmailManifest()
		if err == nil {
			log.Println("Manifest emailed to", f.Mailto.String())
		} else {
			log.Printf("Error emailing manifest to %s: %s", f.Mailto.String(), err)
		}
	} else if f.OutputFile != "" { // write manifest to file
		outputFile, err := os.Create(f.OutputFile.String())
		if err != nil {
			log.Fatalln(err)
		}
		defer outputFile.Close()
		err = f.WriteManifest(outputFile)
		if err != nil { // don't exit on this error
			log.Printf("Error writing manifest to %s: %s", f.OutputFile.String(), err)
		} else {
			log.Println("Manifest written to", f.OutputFile.String())
		}
	} else { // no output file specified - write to stdout
		err = f.WriteManifest(os.Stdout)
		if err != nil {
			log.Fatalln(err) // exit on error
		}
	}

	// print summary
	f.PrintSummary(os.Stderr)
}
