#!/bin/bash

set -ex

git pull
go build fester
sudo cp ./fester /usr/local/bin/
