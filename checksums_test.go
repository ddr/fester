package main

import (
	"os"
	"path/filepath"
	"testing"
)

// TestFesterParseChecksumFile tests Fester.ParseChecksumFile()
func TestFesterParseChecksumFile(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/checksums/checksums_valid.txt")
	testDir := filepath.Dir(f.ChecksumFile.String())
	err := f.ParseChecksumFile()
	t.Logf("Checksums: %s", f.Checksums)
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if len(*f.Checksums) != 2 {
		t.Errorf("Expected 2 checksums, got %d", len(*f.Checksums))
	}
	absPath1 := filepath.Join(testDir, "items/1/file01.jpg")
	checksum1, _ := f.GetChecksum(absPath1)
	if checksum1 != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum1)
	}
	absPath2 := filepath.Join(testDir, "items/1/file02.jpg")
	checksum2, _ := f.GetChecksum(absPath2)
	if checksum2 != "0987654321" {
		t.Errorf("Expected '0987654321', got '%s'", checksum2)
	}
}

// TestFesterParseChecksumFileMissing tests Fester.ParseChecksumFile() with a missing checksum file
func TestFesterParseChecksumFileMissing(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/missing.txt")
	err := f.ParseChecksumFile()
	if err == nil {
		t.Errorf("Expected error, got none")
	}
}

// TestFesterParseChecksumFileInvalid tests Fester.ParseChecksumFile() with an invalid checksum file
func TestFesterParseChecksumFileInvalid(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/checksums/checksums_invalid.txt")
	err := f.ParseChecksumFile()
	if err == nil {
		t.Errorf("Expected error, got none")
	}
}

// TestFesterParseChecksumFileEmpty tests Fester.ParseChecksumFile() with an empty checksum file
func TestFesterParseChecksumFileEmpty(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/checksums/checksums_empty.txt")
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
}

func TestFesterParseChecksumFileWinUNCPaths(t *testing.T) {
	f := NewFester()
	f.ContentVolume = RubensteinArchiveVolume
	f.ChecksumFile.Set("test/checksums/checksums_win_unc_paths_valid.txt")
	f.Verbose = true
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if len(*f.Checksums) != 2 {
		t.Errorf("Expected 2 checksums, got %d", len(*f.Checksums))
	}
	checksum1, _ := f.GetChecksum("//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1/file01.jpg")
	if checksum1 != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum1)
		// t.Errorf("%s", f.Checksums)
	}
	checksum2, _ := f.GetChecksum("//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1/file02.jpg")
	if checksum2 != "0987654321" {
		t.Errorf("Expected '0987654321', got '%s'", checksum2)
	}
}

func TestFesterParseChecksumFileWinUNCVariableCasePaths(t *testing.T) {
	f := NewFester()
	f.ContentVolume = RubensteinArchiveVolume
	f.ChecksumFile.Set("test/checksums/checksums_win_unc_variable_case_paths_valid.txt")
	f.Verbose = true
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if len(*f.Checksums) != 2 {
		t.Errorf("Expected 2 checksums, got %d", len(*f.Checksums))
	}
	checksum1, _ := f.GetChecksum("//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1/file01.jpg")
	if checksum1 != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum1)
		// t.Errorf("%s", f.Checksums)
	}
	checksum2, _ := f.GetChecksum("//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1/file02.jpg")
	if checksum2 != "0987654321" {
		t.Errorf("Expected '0987654321', got '%s'", checksum2)
	}
}

// TestFesterParseChecksumFile tests Fester.ParseChecksumFile()
func TestFesterParseChecksumFileWithBOM(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/checksums/checksums_valid_with_bom.txt")
	testDir := filepath.Dir(f.ChecksumFile.String())
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if len(*f.Checksums) != 2 {
		t.Errorf("Expected 2 checksums, got %d", len(*f.Checksums))
	}
	absPath1 := filepath.Join(testDir, "items/1/file01.jpg")
	checksum1, _ := f.GetChecksum(absPath1)
	if checksum1 != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum1)
	}
	absPath2 := filepath.Join(testDir, "items/1/file02.jpg")
	checksum2, _ := f.GetChecksum(absPath2)
	if checksum2 != "0987654321" {
		t.Errorf("Expected '0987654321', got '%s'", checksum2)
	}
}

// Test checksum file with leading and trailing whitespace
func TestFesterParseChecksumFileWithWhitespace(t *testing.T) {
	f := NewFester()
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	f.ChecksumFile.Set("test/checksums/checksums_valid_with_whitespace.txt")
	testDir := filepath.Dir(f.ChecksumFile.String())
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if len(*f.Checksums) != 2 {
		t.Errorf("Expected 2 checksums, got %d", len(*f.Checksums))
	}
	absPath1 := filepath.Join(testDir, "items/1/file01.jpg")
	checksum1, _ := f.GetChecksum(absPath1)
	if checksum1 != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum1)
	}
	absPath2 := filepath.Join(testDir, "items/1/file02.jpg")
	checksum2, _ := f.GetChecksum(absPath2)
	if checksum2 != "0987654321" {
		t.Errorf("Expected '0987654321', got '%s'", checksum2)
	}
}
