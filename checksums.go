package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"unicode/utf8"
)

var whitespace = regexp.MustCompile(`\s+`)

type Checksums map[string]string // maps paths to checksums

// Get the keys of a Checksums map
func (c *Checksums) Keys() []string {
	keys := make([]string, len(*c))
	i := 0
	for k := range *c {
		keys[i] = k
		i++
	}
	return keys
}

func (c *Checksums) Add(path, checksum string) error {
	if *c == nil {
		*c = make(Checksums)
	}
	if path == "" || checksum == "" {
		return ErrChecksumInvalid
	}
	if _, ok := (*c)[path]; ok { // path already exists
		return fmt.Errorf("duplicate checksum for path: %s", path)
	}
	(*c)[path] = checksum
	return nil
}

func (c *Checksums) Get(path string) (checksum string, ok bool) {
	checksum, ok = (*c)[path]
	return checksum, ok
}

func (f *Fester) HasChecksums() bool {
	return f.Checksums != nil && len(*f.Checksums) > 0
}

func (f *Fester) GetChecksum(path string) (checksum string, found bool) {
	if !f.HasChecksums() {
		return "", false
	}
	checksum, found = f.Checksums.Get(path)
	return checksum, found
}

func (f *Fester) SetChecksums() (err error) {
	if f.ChecksumFile == "" {
		return nil
	}
	return f.ParseChecksumFile()
}

func (f *Fester) ChecksumFileDir() string {
	return filepath.Dir(f.Options.ChecksumFile.String())
}

func (f *Fester) AddChecksum(checksum, path string) (err error) {
	f.debug("adding checksum: %s %s\n", checksum, path)
	path, err = f.ContentVolumePath(path) // normalize path
	if err != nil {
		return err
	}
	err = f.Checksums.Add(path, checksum)
	if err != nil {
		return err
	}
	f.debug("normalized checksum added: %s %s\n", checksum, path)
	return nil
}

func (f *Fester) ParseChecksumFile() error {
	file, err := os.Open(f.ChecksumFile.String())
	if err != nil {
		return err
	}
	defer file.Close()

	// skip the BOM if present
	err = skipBOM(file)
	if err != nil {
		return err
	}

	// relative path must be relative to checksum file!
	wd, err := os.Getwd()
	if err != nil {
		return err
	}
	os.Chdir(f.ChecksumFileDir())
	defer os.Chdir(wd)

	// configure the checksum file scanner
	fileScanner := bufio.NewScanner(file)
	fileScanner.Split(bufio.ScanLines)

	// parse the checksum file
	for lno := 1; fileScanner.Scan(); lno++ {
		line := fileScanner.Text()

		if !utf8.ValidString(line) {
			return fmt.Errorf("invalid UTF-8 at line %d: %s", lno, line)
		}

		// skip empty lines and comment lines
		if isBlankOrCommentLine(line) {
			f.debug("skipping checksum file line %d: %s\n", lno, line)
			continue
		}
		entry := whitespace.Split(strings.TrimSpace(line), 2)
		if len(entry) < 2 {
			return fmt.Errorf("invalid checksum file entry at line %d: %s", lno, line)
		}
		checksum, path := entry[0], entry[1]
		f.debug("checksum file entry: [%s] [%s]\n", checksum, path)

		err = f.AddChecksum(checksum, path)
		if err != nil {
			return err
		}
	}
	return nil
}
