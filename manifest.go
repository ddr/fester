package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

const (
	CaptionFile         = "caption"
	DerivedImageFile    = "derived_image"
	ExtractedTextFile   = "extracted_text"
	FITSFile            = "fits_file"
	IntermediateFile    = "intermediate_file"
	MultiresImageFile   = "multires_image"
	StreamableMediaFile = "streamable_media"
	StructMetadataFile  = "struct_metadata"
	ThumbnailFile       = "thumbnail"

	CollectionModel = "Collection"
	ItemModel       = "Item"
	ComponentModel  = "Component"
	TargetModel     = "Target"
	AttachmentModel = "Attachment"

	FileModelPrefix = "File"
	FileModelSep    = "."

	CaptionFileModel         = FileModelPrefix + FileModelSep + CaptionFile
	DerivedImageFileModel    = FileModelPrefix + FileModelSep + DerivedImageFile
	ExtractedTextFileModel   = FileModelPrefix + FileModelSep + ExtractedTextFile
	FITSFileModel            = FileModelPrefix + FileModelSep + FITSFile
	IntermediateFileModel    = FileModelPrefix + FileModelSep + IntermediateFile
	MultiresImageFileModel   = FileModelPrefix + FileModelSep + MultiresImageFile
	StreamableMediaFileModel = FileModelPrefix + FileModelSep + StreamableMediaFile
	StructMetadataFileModel  = FileModelPrefix + FileModelSep + StructMetadataFile
	ThumbnailFileModel       = FileModelPrefix + FileModelSep + ThumbnailFile

	IdCol         = "id"
	ModelCol      = "model"
	PathCol       = "path"
	ChecksumCol   = "sha1"
	LocalIdCol    = "local_id"
	TitleCol      = "title"
	NestedPathCol = "nested_path"
)

var ResourceModels = []string{
	CollectionModel,
	ItemModel,
	ComponentModel,
	TargetModel,
	AttachmentModel,
}

var FileModels = []string{
	CaptionFileModel,
	DerivedImageFileModel,
	ExtractedTextFileModel,
	FITSFileModel,
	IntermediateFileModel,
	MultiresImageFileModel,
	StreamableMediaFileModel,
	StructMetadataFileModel,
	ThumbnailFileModel,
}

var Models = append(ResourceModels, FileModels...)

var DefaultManifestHeaders = []string{ModelCol, PathCol, ChecksumCol}

func (f *Fester) ManifestRows() (rows []Row) {
	return f.Manifest.Rows
}

func (f *Fester) LastManifestRow() (row *Row) {
	if len(f.ManifestRows()) == 0 {
		return nil
	}
	return &f.ManifestRows()[len(f.ManifestRows())-1]
}

func (f *Fester) ManifestHeaders() (headers []string) {
	return f.Manifest.Headers
}

func (f *Fester) SetManifest() (err error) {
	if !f.MetadataIsSet() {
		return errors.New("metadata is not set")
	}

	// ensure manifest has the default headers
	for _, header := range DefaultManifestHeaders {
		if !f.Manifest.HasHeader(header) {
			f.Manifest.AddHeader(header)
		}
	}

	// merge the headers from the metadata file into the manifest headers
	for _, header := range f.MetadataHeaders() {
		if !f.Manifest.HasHeader(header) {
			f.Manifest.AddHeader(header)
		}
	}

	// ensure local_id header is present for DPC folder packages
	if f.IsDPCFolderPackage() {
		if !f.Manifest.HasHeader(LocalIdCol) {
			f.Manifest.AddHeader(LocalIdCol)
		}
	}

	// ensure nested_path header is present for nested folder packages
	if f.IsNestedFolderPackage() {
		if !f.Manifest.HasHeader(NestedPathCol) {
			f.Manifest.AddHeader(NestedPathCol)
		}
	}

	if f.IsGenericFolderPackage() {
		if f.CollectionPerFolder && !f.Manifest.HasHeader(TitleCol) {
			f.Manifest.AddHeader(TitleCol)
		}
	}

	return nil
}

func (f *Fester) ParseManifestFile() error {
	manifestFile, err := os.Open(f.ManifestFile.String())
	if err != nil {
		return err
	}
	defer manifestFile.Close()

	// skip the BOM if present
	err = skipBOM(manifestFile)
	if err != nil {
		return err
	}

	// read the manifest file
	csvReader := csv.NewReader(manifestFile)
	csvReader.TrimLeadingSpace = true
	//reader.ReuseRecord = true

	// read the headers
	headers, err := csvReader.Read()
	if err != nil {
		return err
	}
	// normalize the headers
	headers, err = normalizeHeaders(headers)
	if err != nil {
		return err
	}
	f.debug("manifest file headers: %v", headers)

	modelColIndex := listIndex(headers, ModelCol)
	if modelColIndex == -1 {
		return ErrManifestMissingModel
	}

	// merge the headers from the manifest file with the default headers
	for _, header := range headers {
		if !f.Manifest.HasHeader(header) {
			f.Manifest.AddHeader(header)
			f.debug("added manifest header: %s", header)
		}
	}

	// read the rows
	for {
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			f.debug("end of manifest file")
			break
		}
		if err != nil {
			return err
		}
		f.debug("manifest file row: %v", csvRow)
		csvRow = normalizeRow(csvRow)
		f.debug("manifest file row normalized: %v", csvRow)

		row := Row{}
		for i, header := range headers { // use only the headers from the manifest file
			row.Set(header, csvRow[i])
		}

		// if file model and id is present, add a Component row with the id
		// and remove the id from the file row
		if row.IsFile() && row.HasId() {
			component := Row{}
			component.Set(ModelCol, ComponentModel)
			component.Set(IdCol, row.Id())
			f.AddManifestRow(&component)
			row.Set(IdCol, "")
		}

		f.debug("manifest row: %v", row)
		err = f.AddManifestRow(&row)
		if err != nil {
			return err
		}
	}

	return nil
}

func IsValidModel(model string) bool {
	return listContains(Models, model)
}

func IsFileModel(model string) bool {
	return listContains(FileModels, model)
}

func (f *Fester) DuplicateFileMessage(checksum string) {
	msg := fmt.Sprintf(ErrChecksumNotUnique, checksum)
	f.Message(msg, DuplicateFileMessageType)
}

func (f *Fester) ValidateContentResource(r *Row) (err error) {
	if r.HasPath() {
		// path (new resource) and id (existing resource) may not both be present
		if r.HasId() {
			return fmt.Errorf(ErrContentResourcePathAndIdPresent, *r)
		}
		// path must be unique
		if f.IsDuplicateManifestPath(r.Path()) {
			return fmt.Errorf(ErrPathNotUnique, r.Path())
		}
		// checksum must be unique, if present
		if f.IsDuplicateManifestChecksum(r.Checksum()) {
			f.DuplicateFileMessage(r.Checksum())
		}
		return nil
	}

	// path is not present
	if !r.HasId() { // path or id should be present
		return fmt.Errorf(ErrContentResourcePathOrIdRequired, *r)
	}

	return nil
}

func (f *Fester) ValidateFileRow(r *Row) (err error) {
	// File row must be preceded by Item or Component row
	if len(f.ManifestRows()) == 0 {
		return fmt.Errorf(ErrFileWithoutResource, r)
	}
	// path must be present
	if !r.HasPath() {
		return fmt.Errorf(ErrPathMissing, r)
	}
	// path must be unique
	if f.IsDuplicateManifestPath(r.Path()) {
		return fmt.Errorf(ErrPathNotUnique, r.Path())
	}
	// checksum must be unique, if present
	if f.IsDuplicateManifestChecksum(r.Checksum()) {
		f.DuplicateFileMessage(r.Checksum())
	}
	return nil
}

func (f *Fester) ValidateCollectionRow(r *Row) (err error) {
	f.debug("validating collection row: %v", *r)

	// A Collection row must have either a title or an id
	if !r.HasTitle() && !r.HasId() {
		return fmt.Errorf(ErrCollectionTitleOrIdRequired, r)
	}
	return nil
}

func (f *Fester) ValidateComponentRow(r *Row) (err error) {
	// Component row must be preceded by Item row, except for generic folder package
	// if !r.HasId() && f.lastItemRow == -1 && !f.IsGenericFolderPackage() {
	// 	return fmt.Errorf(ErrComponentIdOrParentRequired, *r)
	// }
	// checksum is required for new Components
	if r.HasPath() && !r.HasChecksum() {
		return fmt.Errorf(ErrChecksumMissing, r)
	}
	// DPC Folder package requires Components to have a unique local id
	// if f.IsDPCFolderPackage() && f.IsDuplicateManifestLocalId(r.LocalId()) {
	// 	return fmt.Errorf(ErrLocalIdNotUnique, r.LocalId())
	// }
	// must satisfy content resource validation rules
	return f.ValidateContentResource(r)
}

func (f *Fester) ValidateItemRow(r *Row) (err error) {
	// no validation rules for Item rows
	return nil
}

func (f *Fester) ValidateManifestRow(r *Row) (err error) {
	f.debug("validating manifest row: %v", *r)

	if f.IsDuplicateManifestId(r.Id()) {
		return fmt.Errorf(ErrIdNotUnique, r.Id())
	}

	switch r.Model() {
	case CollectionModel:
		return f.ValidateCollectionRow(r)
	case ItemModel:
		return f.ValidateItemRow(r)
	case ComponentModel:
		return f.ValidateComponentRow(r)
	case TargetModel:
		return f.ValidateContentResource(r)
	case AttachmentModel:
		f.debug("attachment row: %v", *r)
		return f.ValidateContentResource(r)
	}

	if r.IsFile() {
		return f.ValidateFileRow(r)
	}

	return fmt.Errorf("invalid model: %s", *r)
}

func (f *Fester) MergeMetadata(row *Row, metadata *Row) {
	for col, value := range *metadata {
		if value == "" || col == PathCol || row.HasValue(col) {
			f.debug("Skipping metadata column: %s, value: %s", col, value)
			continue
		}
		row.Set(col, value)
	}
}

func (f *Fester) AddManifestContent() error {
	return filepath.WalkDir(f.ContentPath(), f.HandleContentPath)
}

func (f *Fester) GenerateManifest() (err error) {
	if f.Root == "" { // ensure root is set, default to current working directory
		f.Root.Set(".")
	}

	f.SetContentVolume()

	err = f.SetChecksums()
	if err != nil {
		return err
	}

	err = f.SetMetadata()
	if err != nil {
		return err
	}

	err = f.SetManifest()
	if err != nil {
		return err
	}

	// parse provided manifest file, if present
	if f.ManifestFile.String() != "" {
		err = f.ParseManifestFile()
		if err != nil {
			return err
		}
	}

	f.SetNonContentPaths()
	f.SetComponentFiles()

	// Only add items and components to the manifest if the package type is set.
	// Not setting the package type is only valid when a manifest file is provided.
	if f.PackageType.String() != "" {
		err = f.AddManifestContent()
		if err != nil {
			return err
		}
	}

	f.AddManifestTargets()

	return nil
}

func (f *Fester) HandleDPCFolderPath(path string, d os.DirEntry) (err error) {
	f.debug("DPC Folder - handling path: %s\n", path)

	if d.IsDir() {
		if f.CollectionPerFolder && f.IsTopLevelContentDir(path) {
			f.debug("DPC Folder - new collection: %s\n", path)

			collection := f.NewCollectionRow()
			collection.SetLocalId(d.Name()) // use directory name as local id
			f.SetRowPath(collection, path)

			metadata, ok := f.GetMetadataRowByKey(collection.LocalId())
			if ok {
				f.MergeMetadata(collection, metadata)
			} else {
				// use directory name as provisional title
				// b/c we need either a title or an id for the collection
				collection.SetTitle(d.Name())
			}

			err = f.AddManifestRow(collection)
			if err != nil {
				return err
			}
		}

		// This doesn't work b/c the target files are not currently named
		// in this way. There may be conversations about changing the naming
		// convention for target files, so leaving this here for now.

		// if f.TargetFiles.String() != "" {
		// 	// check if there is a target file for this directory and, if so, add it
		// 	targetPath := filepath.Join(f.TargetFiles.String(), d.Name(), TargetFileExtension)
		// 	if _, fileErr := os.Stat(targetPath); fileErr == nil {
		// 		f.AddManifestTarget(targetPath)
		// 	}
		// }

		return nil
	}

	componentLocalId := filePrefix(path) // base name of file without extension

	itemLocalId := componentLocalId
	if f.HasItemIdLength() && len(componentLocalId) > f.ItemIdLength {
		itemLocalId = componentLocalId[:f.ItemIdLength]
	}

	// if the item id is different from the last item id, it's a new item
	if itemLocalId != f.lastItemLocalId {
		f.debug("DPC Folder - new item: %s\n", itemLocalId)
		f.lastItemLocalId = itemLocalId // update lastItemLocalId
		item := f.NewItemRow()
		// if f.CollectionPerFolder {
		// 	f.SetRowPath(item, path)
		// }
		item.SetLocalId(itemLocalId)
		if metadata, ok := f.GetMetadataRowByKey(itemLocalId); ok {
			f.MergeMetadata(item, metadata)
		}
		err = f.AddManifestRow(item)
		if err != nil {
			return err
		}
	}

	// add component
	f.debug("DPC Folder - new component: %s\n", componentLocalId)
	component, err := f.NewComponentRow(path)
	if err != nil {
		return err
	}
	component.SetLocalId(componentLocalId)
	// don't associate metadata with components if item id length is 0
	// b/c 1-1 Item-Component relationship
	if f.HasItemIdLength() {
		if metadata, ok := f.GetMetadataRowByKey(componentLocalId); ok {
			f.MergeMetadata(component, metadata)
		}
	}
	err = f.AddManifestRow(component)
	if err != nil {
		return err
	}

	// add component files
	err = f.AddManifestComponentFiles(path)
	if err != nil {
		return err
	}

	return nil
}

func (f *Fester) HandleStandardPackagePath(path string, d os.DirEntry) (err error) {
	f.debug("Standard package - handling path: %s\n", path)

	if d.IsDir() { // directory = Item in standard ingest
		f.debug("Standard package - new item: %s\n", path)
		item := f.NewItemRow()
		if metadata, ok := f.GetMetadataRowByKey(path); ok {
			f.MergeMetadata(item, metadata)
		}
		return f.AddManifestRow(item)
	}
	// file = Component in standard ingest
	f.debug("Standard package - new component: %s\n", path)
	err = f.AddManifestComponent(path)
	if err != nil {
		return err
	}
	return f.AddManifestComponentFiles(path)
}

func (f *Fester) RelativeContentPath(path string) (relPath string, err error) {
	relPath, err = filepath.Rel(f.ContentPath(), path)
	if err != nil {
		return "", err
	}
	return relPath, nil
}

func (f *Fester) HandleNestedFolderPath(path string, d os.DirEntry) (err error) {
	f.debug("Nested folder - handling path: %s\n", path)
	//
	// In nested folder ingest every file represents an Item and its single Component.
	// Directories are not meaningful.
	//
	if d.IsDir() {
		f.debug("Nested folder - skipping directory: %s\n", path)
		return nil
	}
	item := f.NewItemRow()
	// look up metadata by path, but don't set the path on the Item
	if metadata, ok := f.GetMetadataRowByKey(path); ok {
		f.MergeMetadata(item, metadata)
	}
	relPath, err := f.RelativeContentPath(path)
	if err != nil {
		return err
	} else {
		item.SetNestedPath(relPath)
	}
	err = f.AddManifestRow(item)
	if err != nil {
		return err
	}

	err = f.AddManifestComponent(path)
	if err != nil {
		return err
	}
	err = f.AddManifestComponentFiles(path)
	if err != nil {
		return err
	}

	return nil
}

func (f *Fester) IsTopLevelContentDir(path string) bool {
	return filepath.Dir(path) == f.ContentPath()
}

func (f *Fester) HandleGenericFolderPath(path string, d os.DirEntry) (err error) {
	f.debug("Generic folder - handling path: %s\n", path)

	if d.IsDir() {
		// if f.CollectionPerFolder && f.IsTopLevelContentDir(path) {
		// 	f.debug("Generic folder - new collection: %s\n", path)
		// 	collection := f.NewCollection()
		// 	collection.SetTitle(d.Name()) // use directory name as title
		// 	err = f.AddManifestRow(collection)
		// 	if err != nil {
		// 		return err
		// 	}
		// }
		return nil
	}

	// if the last row added was a collection, then we
	// need to add an item row before adding the component

	// lastManifestRow := f.LastManifestRow()
	// if lastManifestRow != nil && lastManifestRow.IsCollection() {
	// 	f.debug("Generic folder - new item: %s\n", path)
	// 	item := f.NewItem()
	// 	// use directory path for item metadata key
	// 	metadata, ok := f.GetMetadataRowByKey(filepath.Dir(path))
	// 	if ok {
	// 		f.MergeMetadata(item, metadata)
	// 	} else {
	// 		// use directory name as provisional title
	// 		item.SetTitle(filepath.Base(filepath.Dir(path)))
	// 	}
	// 	err = f.AddManifestRow(item)
	// 	if err != nil {
	// 		return err
	// 	}
	// }

	err = f.AddManifestComponent(path)
	if err != nil {
		return err
	}
	err = f.AddManifestComponentFiles(path)
	if err != nil {
		return err
	}

	return nil
}

func (f *Fester) HandleContentPath(path string, d os.DirEntry, err error) error {
	f.debug("Content path: %s\n", path)

	if err != nil {
		return err
	}

	if path == f.ContentPath() { // don't process the root content directory
		return nil
	}

	if !d.IsDir() {
		info, err := d.Info()
		if err != nil {
			return err
		}
		if info.Size() == 0 { // don't process empty files
			f.Warning(fmt.Errorf("skipping empty file: %s", path))
			return nil
		}
	}

	if f.IsNonContentPath(path) {
		f.debug("skipping non-content path: %s\n", path)
		if d.IsDir() { // don't descend into non-content dirs
			return filepath.SkipDir
		} else { // file
			return nil
		}
	}

	if f.IsExcludedContentPath(d, path) {
		f.debug("skipping excluded content path: %s\n", path)
		f.ExcludePath(path)
		if d.IsDir() { // don't descend into excluded dirs
			return filepath.SkipDir
		} else { // file
			return nil
		}
	}

	switch f.PackageType.String() {
	case DPCFolderPackage:
		return f.HandleDPCFolderPath(path, d)
	case StandardPackage:
		return f.HandleStandardPackagePath(path, d)
	case NestedFolderPackage:
		return f.HandleNestedFolderPath(path, d)
	case GenericFolderPackage:
		return f.HandleGenericFolderPath(path, d)
	}

	return nil
}

func (f *Fester) NewComponentRow(path string) (component *Row, err error) {
	component = NewModelRow(ComponentModel)
	f.SetRowPath(component, path)
	err = f.SetRowChecksum(component, true) // checksum required for Component
	return component, err
}

func (f *Fester) NewTargetRow(path string) (target *Row) {
	target = NewModelRow(TargetModel)
	f.SetRowPath(target, path)
	f.SetRowChecksum(target, false) // checksum not required for Target
	return target
}

func (f *Fester) NewItemRow() *Row {
	return NewModelRow(ItemModel)
}

func (f *Fester) NewCollectionRow() *Row {
	return NewModelRow(CollectionModel)
}

func NewModelRow(model string) *Row {
	row := NewRow()
	row.SetModel(model)
	return row
}

func (f *Fester) SetRowPath(row *Row, path string) error {
	volPath, err := f.ContentVolumePath(path)
	if err != nil {
		return err
	}
	row.SetPath(volPath)
	return nil
}

func (f *Fester) SetRowChecksum(row *Row, required bool) error {
	checksum, found := f.GetChecksum(row.Path())
	if found {
		row.SetChecksum(checksum)
		return nil
	}
	if required {
		// if f.Debug {
		// 	fmt.Println("Checksum keys:")
		// 	for key := range *f.Checksums {
		// 		fmt.Println(key)
		// 	}
		// }
		return fmt.Errorf("checksum not found for Component content file %s", row.Path())
	}
	return nil
}

func (f *Fester) SetMetadataKeyValue(row *Row, key string) {
	row.Set(f.MetadataKey(), key)
}

func (f *Fester) GetMetadataKeyValue(row *Row) (value string, found bool) {
	value, found = row.Get(f.MetadataKey())
	return value, found
}

func (f *Fester) AddManifestComponent(path string) error {
	f.debug("Adding Component: %s\n", path)
	component, err := f.NewComponentRow(path)
	if err != nil {
		return err
	}
	if !f.IsNestedFolderPackage() { // nested folder package can only have item metadata
		if metadata, ok := f.GetMetadataRowByKey(component.Path()); ok {
			f.MergeMetadata(component, metadata)
		}
	}
	return f.AddManifestRow(component)
}

// Appends a row to the manifest
func (f *Fester) AddManifestRow(row *Row) (err error) {
	// f.debug("Normalizing manifest row model: %s\n", *row)
	err = row.NormalizeModel()
	if err != nil {
		return err
	}

	// f.debug("Validating manifest row: %s\n", *row)
	err = f.ValidateManifestRow(row)
	if err != nil {
		return err
	}

	f.debug("Adding manifest row: %s\n", *row)
	index := f.Manifest.AddRow(*row)
	f.debug("Added manifest row: %s\n", *row)

	switch {
	case row.IsItem():
		f.lastItemRow = index
	case row.IsComponent():
		f.lastComponentRow = index
	}

	return nil
}

// Appends a manifest row for a Component file
func (f *Fester) AddManifestComponentFile(model string, path string) error {
	f.debug("Adding Component %s file: %s\n", model, path)
	file := NewModelRow(model)
	f.SetRowPath(file, path)
	f.SetRowChecksum(file, false) // checksum not required for Component file
	return f.AddManifestRow(file)
}

// Appends manifest rows for Component files (if any)
func (f *Fester) AddManifestComponentFiles(componentPath string) (err error) {
	componentPathPrefix := filePrefix(componentPath)

	for model, paths := range f.componentFiles {
		for _, path := range paths {
			if filePrefix(path) == componentPathPrefix {
				err = f.AddManifestComponentFile(model, path)
				if err != nil {
					return err
				}
				break // only add one file per model-component pair
			}
		}
	}
	return nil
}

func (f *Fester) AddManifestTarget(path string) error {
	target := f.NewTargetRow(path)
	return f.AddManifestRow(target)
}

// Returns the paths of the files to be added as Targets,
// removing any paths that are already present in the manifest.
func (f *Fester) TargetPathsToAdd() []string {
	if f.TargetFiles.String() == "" {
		return []string{}
	}
	pattern := filepath.Join(f.TargetFiles.String(), TargetFilePattern)
	matches, err := filepath.Glob(pattern)
	if err != nil { // only bad pattern
		panic(err)
	}
	return matches

	// Leaving this here in case we want to add a check for existing targets
	// in the future.  Currently, all target are added to the end of the manifest.

	// var existingTargetPaths []string
	// for _, row := range f.ManifestRowsByModel(TargetModel) {
	// 	existingTargetPaths = append(existingTargetPaths, row.Path())
	// }
	// if len(existingTargetPaths) == 0 {
	// 	// no existing targets, return all matches
	// 	return matches
	// }
	// paths := []string{}
	// for _, match := range matches {
	// 	matchContentVolumePath, err := f.ContentVolumePath(match)
	// 	if err != nil {
	// 		panic(err)
	// 	}
	// 	if !listContains(existingTargetPaths, matchContentVolumePath) {
	// 		paths = append(paths, matchContentVolumePath)
	// 	}
	// }
	// return paths
}

func (f *Fester) AddManifestTargets() {
	for _, path := range f.TargetPathsToAdd() {
		f.AddManifestTarget(path)
	}
}

func (f *Fester) IsDuplicateManifestPath(path string) bool {
	return f.Manifest.IsDuplicateValue(PathCol, path)
}

func (f *Fester) IsDuplicateManifestId(id string) bool {
	return f.Manifest.IsDuplicateValue(IdCol, id)
}

func (f *Fester) IsDuplicateManifestChecksum(checksum string) bool {
	return f.Manifest.IsDuplicateValue(ChecksumCol, checksum)
}

func (f *Fester) IsDuplicateManifestLocalId(localId string) bool {
	return f.Manifest.IsDuplicateValue(LocalIdCol, localId)
}
