# fester Change Log

## v1.2.0

- Allows other component file folders to have subfolders (DDK-81).
- Added `-collection-per-folder` option to support generating a manifest for multiple collections at once (DDK-67; DPC Folder package type only).
- Changed `-mailto` option to permit multiple Duke email addresses.

## v1.1.1

- Checksum file lines are validated for UTF-8 encoding (due to DDK-57)
- Adds debug logging

## v1.1.0

- Adds ability to input an existing manifest file without generating new manifest entries from source files by not specifying a package type.

## v1.0.1

- Fixes bug in handling relative paths in the metadata file.

## v1.0.0

Initial production release.