package main

import (
	"os"
	"testing"
)

// TestFesterParseMetadataFileMissing tests Fester.ParseMetadataFile() with a missing metadata file
func TestMetadataParseFileMissing(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/missing.txt")
	err := f.SetMetadata()
	if err == nil {
		t.Errorf("Expected error, got none")
	}
}

// TestFesterParseMetadataFileInvalid tests Fester.ParseMetadataFile() with an invalid metadata file
func TestMetadataParseFileInvalid(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_invalid.csv")
	err := f.SetMetadata()
	if err == nil {
		t.Errorf("Expected error, got none")
	}
}

// TestFesterParseMetadataFile tests Fester.ParseMetadataFile()
func TestMetadataParseCSVFile(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid.csv")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
}

// TestFesterParseTSVMetadataFile tests Fester.ParseMetadataFile()
func TestMetadataParseTSVFile(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_tsv.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
}

func TestMetadataParseFileWithBOM(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_with_bom.csv")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
}

func TestMetadataParseFileWithMixedCaseHeaders(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_mixed_case_headers.csv")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if !listContains(f.MetadataHeaders(), "path") {
		t.Errorf("Expected path header, got none")
	}
	if !listContains(f.MetadataHeaders(), "title") {
		t.Errorf("Expected title header, got none")
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
}

// test metadata file with UNC paths
func TestMetadataParseFileWithUNCPaths(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_unc_paths.csv")
	f.ContentVolume = RubensteinArchiveVolume
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if !listContains(f.MetadataHeaders(), "path") {
		t.Errorf("Expected path header, got none")
	}
	if !listContains(f.MetadataHeaders(), "title") {
		t.Errorf("Expected title header, got none")
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
	if f.MetadataRows()[0]["path"] != "//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1', got %s", f.MetadataRows()[0]["path"])
	}
}

func TestMetadataParseFileWithWinUNCPaths(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_win_unc_paths.csv")
	f.ContentVolume = RubensteinArchiveVolume
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if !listContains(f.MetadataHeaders(), "path") {
		t.Errorf("Expected path header, got none")
	}
	if !listContains(f.MetadataHeaders(), "title") {
		t.Errorf("Expected title header, got none")
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
	if f.MetadataRows()[0]["path"] != "//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/RL-Archive/items/1', got %s", f.MetadataRows()[0]["path"])
	}
}

// test parse metadata file with extra whitespace
func TestMetadataParseFileWithExtraWhitespace(t *testing.T) {
	f := NewFester()
	f.MetadataFile.Set("test/metadata/metadata_valid_extra_whitespace.csv")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.SetMetadata()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.MetadataHeaders()) != 2 {
		t.Errorf("Expected 2 metadata headers, got %d", len(f.MetadataHeaders()))
	}
	if len(f.MetadataRows()) != 3 {
		t.Errorf("Expected 3 metadata rows, got %d", len(f.MetadataRows()))
	}
	if !listContains(f.MetadataHeaders(), "path") {
		t.Errorf("Expected path header, got none")
	}
}
