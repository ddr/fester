package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

const (
	PathListSeparator = string(filepath.ListSeparator)

	DPCFolderPackage     = "dpc"
	StandardPackage      = "standard"
	NestedFolderPackage  = "nested"
	GenericFolderPackage = "generic"

	JSONIndent = "    " // 4 spaces
)

// Global vars
var DukeEmailPattern = regexp.MustCompile(`\A[a-z][\w\.\-]+@duke\.edu\z`)
var PackageTypes = []string{DPCFolderPackage, StandardPackage, NestedFolderPackage, GenericFolderPackage}
var DefaultExcludePathNames = PathList{".*", "Thumbs.db"} // exclude hidden files and Thumbs.db

// PackageType is a flag.Value for the package type
type PackageType string

func (p *PackageType) String() string {
	return string(*p)
}

func (p *PackageType) Set(value string) error {
	*p = PackageType(value)
	if err := p.Validate(); err != nil {
		return err
	}
	return nil
}

func (p *PackageType) IsEmpty() bool {
	return p == nil || p.String() == ""
}

func (p *PackageType) Validate() error {
	if !p.IsEmpty() && !listContains(PackageTypes, p.String()) {
		return fmt.Errorf("invalid package type: %s", p.String())
	}
	return nil
}

// DukeEmail is a flag.Value for Duke email addresses
type DukeEmail string

func (e *DukeEmail) Set(value string) error {
	*e = DukeEmail(value)
	if err := e.Validate(); err != nil {
		return err
	}
	return nil
}

func (e *DukeEmail) String() string {
	return string(*e)
}

func (e *DukeEmail) IsEmpty() bool {
	return e == nil || e.String() == ""
}

func (e *DukeEmail) Validate() error {
	if e.IsEmpty() {
		return nil
	}
	if !DukeEmailPattern.MatchString(e.String()) {
		return errors.New("invalid Duke email address: " + e.String())
	}
	return nil
}

type DukeEmailList []DukeEmail

func (e *DukeEmailList) Set(value string) error {
	emails := strings.Split(value, ",")
	*e = make(DukeEmailList, len(emails))
	for i, email := range emails {
		if email == "" { // skip empty email addresses
			continue
		}
		if err := (*e)[i].Set(email); err != nil {
			return err
		}
	}
	return nil
}

func (e *DukeEmailList) String() string {
	emails := make([]string, len(*e))
	for i, email := range *e {
		emails[i] = string(email)
	}
	return strings.Join(emails, ",")
}

func (e *DukeEmailList) Validate() error {
	for _, email := range *e {
		if err := email.Validate(); err != nil {
			return err
		}
	}
	return nil
}

// AbsPath is a flag.Value for coercing a path to an absolute path
// based on current working directory.
type AbsPath string

func (p *AbsPath) Set(value string) error {
	abs, err := filepath.Abs(value)
	if err != nil {
		return err
	}
	*p = AbsPath(abs)
	return nil
}

func (p *AbsPath) String() string {
	return string(*p)
}

func (p *AbsPath) IsEmpty() bool {
	return p == nil || p.String() == ""
}

func (p *AbsPath) Exists() bool {
	if p.IsEmpty() {
		return false
	}
	_, err := os.Stat(p.String())
	return err == nil
}

func (p *AbsPath) Validate() error {
	if p.IsEmpty() {
		return nil
	}
	if !p.Exists() {
		return fmt.Errorf("path does not exist: %s", *p)
	}
	return nil
}

// PathList is a flag.Value for a list of paths.
// The input/output list is a string of paths separated by the OS-specific path list separator.
type PathList []string

func (p *PathList) Set(value string) error {
	*p = filepath.SplitList(value)
	return nil
}

func (p *PathList) String() string {
	return strings.Join(*p, PathListSeparator)
}

type AbsPathList []AbsPath

var DefaultExcludeContentPaths AbsPathList

func (p *AbsPathList) Set(value string) error {
	paths := filepath.SplitList(value)
	*p = make(AbsPathList, len(paths))
	for i, path := range paths {
		if path == "" { // skip empty paths
			continue
		}
		if err := (*p)[i].Set(path); err != nil {
			return err
		}
	}
	return nil
}

func (p *AbsPathList) String() string {
	paths := make([]string, len(*p))
	for i, path := range *p {
		paths[i] = string(path)
	}
	return strings.Join(paths, PathListSeparator)
}

func (p *AbsPathList) Validate() error {
	for _, path := range *p {
		if err := path.Validate(); err != nil {
			return err
		}
	}
	return nil
}

type Options struct {
	Root                 AbsPath
	CaptionFiles         AbsPath
	ChecksumFile         AbsPath
	CollectionPerFolder  bool
	ContentFiles         AbsPath
	Debug                bool
	DerivedImageFiles    AbsPath
	Mailto               DukeEmailList
	ExcludePathNames     PathList
	ExcludeContentPaths  AbsPathList
	ExtractedTextFiles   AbsPath
	IntermediateFiles    AbsPath
	ItemIdLength         int
	ManifestFile         AbsPath
	MetadataFile         AbsPath
	MultiresImageFiles   AbsPath
	NonInteractive       bool
	OutputFile           AbsPath
	PackageType          PackageType
	StreamableMediaFiles AbsPath
	TargetFiles          AbsPath
	ThumbnailFiles       AbsPath
	Verbose              bool
}

func init() {
	var defaultConfigFile AbsPath
	defaultConfigFile.Set(DefaultConfigFile)
	DefaultExcludeContentPaths = AbsPathList{defaultConfigFile}
}

func NewOptions() *Options {
	return &Options{
		ExcludePathNames:    DefaultExcludePathNames,
		ExcludeContentPaths: DefaultExcludeContentPaths,
	}
}

type CommandLine struct {
	*Options
	*flag.FlagSet
	ConfigFile     string
	GenerateConfig bool
	Version        bool
}

func NewCommandLine() (c *CommandLine) {
	c = &CommandLine{
		Options:    NewOptions(),
		ConfigFile: DefaultConfigFile,
		FlagSet:    flag.NewFlagSet("fester", flag.ExitOnError),
	}

	c.StringVar(&c.ConfigFile, "config-file", c.ConfigFile,
		"Path to config file, or '-' for STDIN")
	c.BoolVar(&c.GenerateConfig, "generate-config", false,
		"Generate a config file, using the current options, and exit.")
	c.Var(&c.ChecksumFile, "checksum-file", fileOptionMessage("checksum file"))
	c.Var(&c.CaptionFiles, "caption-files", dirOptionMessage("caption files"))
	c.BoolVar(&c.CollectionPerFolder, "collection-per-folder", false,
		"DPC Folder package ONLY: Create a collection per top-level (sub-)folder"+
			"\nwithin the root path or -content-files path (default: false).")
	c.Var(&c.ContentFiles, "content-files",
		dirOptionMessage("content files")+
			"\nIf this option is not used, the root path is assumed to contain the content files.")
	c.BoolVar(&c.Debug, "debug", false, "Enable debug logging (also implies -verbose).")
	c.Var(&c.DerivedImageFiles, "derived-image-files", dirOptionMessage("derived image files"))
	c.Var(&c.ExcludePathNames, "exclude-path-names",
		"List of file/directory names to exclude from any content directory."+
			"\nMay include shell wildcard patterns (Ex. -exclude-path-names .DS_Store *.txt)."+
			"\nDefault: "+DefaultExcludePathNames.String())
	c.Var(&c.ExcludeContentPaths, "exclude-content-paths",
		"List of content file paths to exclude. May include shell wildcard patterns."+
			"\nEx. -exclude-content-paths foo*.txt"+PathListSeparator+"bar/*.xml."+
			"\nDefault: "+DefaultExcludeContentPaths.String())
	c.Var(&c.ExtractedTextFiles, "extracted-text-files", dirOptionMessage("extracted text files"))
	c.Var(&c.IntermediateFiles, "intermediate-files", dirOptionMessage("intermediate files"))
	c.IntVar(&c.ItemIdLength, "item-id-length", c.ItemIdLength,
		"DPC Folder package ONLY: Item local ID length as part of file name."+
			"\n0 (default) means use full name -- i.e., one Component per Item.")
	c.Var(&c.Mailto, "mailto",
		"Email manifest as an attachment to the Duke address(es) supplied."+
			"\nSeparate multiple addresses with commas.")
	c.Var(&c.ManifestFile, "manifest-file", "Path to an existing manifest to prepend to a new manifest.")
	c.Var(&c.MetadataFile, "metadata-file", fileOptionMessage("metadata file"))
	c.Var(&c.MultiresImageFiles, "multires-image-files", dirOptionMessage("multires image files"))
	c.BoolVar(&c.NonInteractive, "non-interactive", c.NonInteractive,
		"Non-interactive mode (do not pause/prompt to continue).")
	c.Var(&c.OutputFile, "output-file",
		"Output manifest or config (if using -generate-config) to file"+
			"\n(full path or relative to working directory)."+
			"\nIf using -mailto, -output-file specifies the name of the attachment."+
			"\nOtherwise, if -output-file is not specified, the manifest is written to STDOUT.")
	c.Var(&c.PackageType, "package-type",
		fmt.Sprintf("Package type: %s", PackageTypes))
	c.Var(&c.StreamableMediaFiles, "streamable-media-files",
		dirOptionMessage("streamable media files"))
	c.Var(&c.TargetFiles, "target-files", dirOptionMessage("target files"))
	c.Var(&c.ThumbnailFiles, "thumbnail-files", dirOptionMessage("thumbnail files"))
	c.BoolVar(&c.Verbose, "verbose", c.Verbose,
		"Verbose output -- i.e., print non-fatal errors and/or warnings to STDERR.")
	c.BoolVar(&c.Version, "version", false, "Print version and exit.")

	c.Usage = func() {
		fmt.Fprintln(c.Output(), "+------------------------------------------------------------+")
		fmt.Fprintln(c.Output(), "| Fester: Duke Digital Repository Package Manifest Generator |")
		fmt.Fprintln(c.Output(), "+------------------------------------------------------------+")
		fmt.Fprintf(c.Output(), "Usage: %s -package-type %s [options] <root path>\n", c.Name(), PackageTypes)
		fmt.Fprintf(c.Output(), "       %s -generate-config [options] <root path>\n", c.Name())
		fmt.Fprintf(c.Output(), "       %s -manifest-file <file path>\n", c.Name())
		fmt.Fprintf(c.Output(), "       %s -help\n", c.Name())
		fmt.Fprintf(c.Output(), "       %s -version\n", c.Name())
		fmt.Fprintln(c.Output(), `
<root path> is the directory containing the content files, unless the '-content-files'
option is used, and defaults to the working directory (i.e., ".").
			
One of the options '-package-type', '-generate-config', or '-manifest-file' must be specified.
			
If the '-manifest-file' option is used without '-package-type', the manifest is read from
the specified file, normalized, validated, and written to STDOUT (or -output-file, if used).

All of the command options (below) may be specified in a JSON config file. 
The default config file location is ".fester.json" in the working directory. 
The '-config-file' option may be used to specify an alternate config file,
or "-" to read the config file from STDIN. 

Run "fester -generate-config" to generate a config file with the default options.
Additional command options are merged with the default options, and the
resulting config file is written to STDOUT.  The '-output-file' option may be
used to write the config file to a file.
			
OPTIONS:`)

		c.PrintDefaults()
	}

	return c
}

func (c *CommandLine) ParseCommand() error {
	if err := c.Parse(os.Args[1:]); err != nil {
		return err
	}
	if c.Version {
		fmt.Println(Version)
		os.Exit(0)
	}
	if c.NArg() > 0 {
		c.Root.Set(c.Arg(0))
	} else {
		c.Root.Set(".")
	}
	return nil
}

func (o *Options) Load(configFile string) {
	switch configFile {
	case "-":
		o.LoadIO(os.Stdin)
	case "":
		o.LoadDefaultPath()
	default:
		o.LoadPath(configFile)
	}
}

func (o *Options) LoadBytes(jsonOpts []byte) error {
	return json.Unmarshal(jsonOpts, o)
}

func (o *Options) LoadIO(r io.Reader) error {
	jsonOpts, err := io.ReadAll(r)
	if err != nil {
		return err
	}
	return o.LoadBytes(jsonOpts)
}

func (o *Options) LoadPath(path string) error {
	jsonOpts, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	return o.LoadBytes(jsonOpts)
}

func (o *Options) LoadDefaultPath() error {
	if _, err := os.Stat(DefaultConfigFile); err == nil {
		return o.LoadPath(DefaultConfigFile)
	}
	return nil
}

func (o *Options) Dump() ([]byte, error) {
	return json.MarshalIndent(*o, "", JSONIndent)
}

func (o *Options) String() string {
	output, err := o.Dump()
	if err != nil {
		panic(err)
	}
	return string(output)
}

func (o *Options) Write(w io.Writer) (err error) {
	output, err := o.Dump()
	if err != nil {
		return err
	}
	output = append(output, []byte("\n")...)
	_, err = w.Write(output)
	return err
}

func (o *Options) Validate() (validationErrors []error) {
	var err error

	err = o.PackageType.Validate()
	if err != nil {
		validationErrors = append(validationErrors, err)
	}

	// -collection-per-folder is DPC folder only
	if o.CollectionPerFolder && o.PackageType != "" && o.PackageType != DPCFolderPackage {
		validationErrors = append(validationErrors, ErrCollectionPerFolderNotSupported)
	}

	if o.ManifestFile.IsEmpty() {
		// if no manifest file, then root path and package type are required
		if o.Root.IsEmpty() {
			validationErrors = append(validationErrors, ErrRootRequired)
		}
		if o.PackageType.IsEmpty() {
			validationErrors = append(validationErrors, ErrPackageTypeRequired)
		}
	}

	if o.ItemIdLength < 0 {
		validationErrors = append(validationErrors, ErrItemIdLengthInvalid)
	}
	if o.ItemIdLength != 0 && o.PackageType != DPCFolderPackage {
		validationErrors = append(validationErrors, ErrItemIdLengthNotSupported)
	}

	err = o.Mailto.Validate()
	if err != nil {
		validationErrors = append(validationErrors, err)
	}

	for _, option := range []AbsPath{
		o.Root,
		o.ContentFiles,
		o.CaptionFiles,
		o.ChecksumFile,
		o.DerivedImageFiles,
		o.ExtractedTextFiles,
		o.IntermediateFiles,
		o.ManifestFile,
		o.MetadataFile,
		o.MultiresImageFiles,
		o.StreamableMediaFiles,
		o.TargetFiles,
		o.ThumbnailFiles,
	} {
		err = option.Validate()
		if err != nil {
			validationErrors = append(validationErrors, err)
		}
	}

	return validationErrors
}

func dirOptionMessage(pathType string) string {
	return "Path to directory containing " + pathType + " (full path or relative to working directory)."
}

func fileOptionMessage(pathType string) string {
	return "Path to " + pathType + " (full path or relative to working directory)."
}
