package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strings"
)

func (f *Fester) MetadataKeyCol() int {
	return listIndex(f.MetadataHeaders(), f.MetadataKey())
}

func (f *Fester) HasMetadata() bool {
	return f.Metadata != nil && len(f.Metadata.Rows) > 0
}

func (f *Fester) MetadataIsSet() bool {
	return f.MetadataFile == "" || f.HasMetadata()
}

func (f *Fester) GetMetadataRowByKey(key string) (metadata *Row, found bool) {
	if !f.HasMetadata() {
		return nil, false
	}
	if key == "" {
		return nil, false
	}
	if f.MetadataKeyIsPath() { // normalize path
		key, _ = f.ContentVolumePath(key)
	}
	if index, found := f.metadataIndex[key]; found {
		f.debug("Metadata row %d found by key: %s\n", index, key)
		return &f.MetadataRows()[index], true
	}
	f.debug("Metadata row not found by key: %s\n", key)
	return nil, false
}

func (f *Fester) MetadataHeaders() []string {
	return f.Metadata.Headers
}

func (f *Fester) MetadataRows() []Row {
	return f.Metadata.Rows
}

func (f *Fester) MetadataKeyIsPath() bool {
	return f.MetadataKey() == PathCol
}

func (f *Fester) MetadataKey() string {
	if f.IsDPCFolderPackage() {
		return LocalIdCol
	}
	return PathCol
}

func (f *Fester) SetMetadata() (err error) {
	if f.MetadataFile == "" || f.HasMetadata() {
		return nil
	}
	return f.ParseMetadataFile()
}

func (f *Fester) AddMetadataRow(dataRow []string, line int) (err error) {
	dataRow = normalizeRow(dataRow) // normalize row

	key := dataRow[f.MetadataKeyCol()] // get key value from row

	if key == "" {
		return fmt.Errorf("empty key value in metadata file at line %d", line)
	}

	f.debug("Metadata key: %s\n", key)

	// metadata key is path - convert path to content volume path
	if f.MetadataKeyIsPath() {
		f.debug("Metadata key is path - converting to content volume path\n")
		key, err = f.ContentVolumePath(key)
		if err != nil {
			return err
		}
		dataRow[f.MetadataKeyCol()] = key
	}

	// check for duplicate key
	if _, dup := f.metadataIndex[key]; dup {
		return fmt.Errorf("duplicate key '%s' in metadata file at line %d", key, line)
	}

	row := Row{}
	for i, header := range f.MetadataHeaders() {
		row.Set(header, dataRow[i])
	}

	f.Metadata.AddRow(row)
	f.debug("Added metadata row: %s\n", row)

	// update metadata index
	f.metadataIndex[key] = len(f.Metadata.Rows) - 1
	f.debug("Added metadata index key %s: %d\n", key, f.metadataIndex[key])

	return nil
}

func (f *Fester) SetMetadataHeaders(headers []string) (err error) {
	headers, err = normalizeHeaders(headers)
	if err != nil {
		return err
	}
	if !listContains(headers, f.MetadataKey()) {
		return fmt.Errorf("metadata file does not contain expected key column '%s'", f.MetadataKey())
	}
	f.Metadata.Headers = headers
	return nil
}

func (f *Fester) ParseCSVMetadataFile(file *os.File) (err error) {
	csvReader := csv.NewReader(file)
	csvReader.TrimLeadingSpace = true
	// read header row
	headers, err := csvReader.Read()
	if err != nil { // abort on problem reading headers
		return err
	}
	err = f.SetMetadataHeaders(headers)
	if err != nil {
		return err
	}
	// read data rows
	for {
		dataRow, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil { // parse error reading line
			return err
		}
		line, _ := csvReader.FieldPos(f.MetadataKeyCol())
		err = f.AddMetadataRow(dataRow, line)
		if err != nil {
			return err
		}
	}
	return nil
}

func (f *Fester) ParseTSVMetadataFile(file *os.File) (err error) {
	tsvReader := bufio.NewScanner(file)
	tsvReader.Scan()

	headers := strings.Split(tsvReader.Text(), string('\t'))
	err = f.SetMetadataHeaders(headers)
	if err != nil {
		return err
	}

	for line := 2; tsvReader.Scan(); line++ {
		dataRow := strings.Split(tsvReader.Text(), string('\t'))
		err = f.AddMetadataRow(dataRow, line)
		if err != nil {
			return err
		}
	}

	return nil
}

func (f *Fester) ParseMetadataFile() error {
	// change to content files dir for converting relative paths to absolute, if present
	cwd, err := os.Getwd()
	if err != nil {
		return err
	}
	os.Chdir(f.ContentPath())
	defer os.Chdir(cwd)

	file, err := os.Open(f.MetadataFile.String())
	if err != nil {
		return err
	}
	defer file.Close()

	// detect TSV or CSV
	buf := make([]byte, 32)
	n, err := file.Read(buf)
	if err != nil && err != io.EOF {
		return err
	}
	if n == 0 {
		return fmt.Errorf("empty metadata file")
	}
	tsv := bytes.Contains(buf, []byte("\t"))
	_, err = file.Seek(0, 0)
	if err != nil {
		return err
	}

	// skip BOM if present - AFTER detecting TSV or CSV
	err = skipBOM(file)
	if err != nil {
		return err
	}

	if tsv {
		return f.ParseTSVMetadataFile(file)
	} else {
		return f.ParseCSVMetadataFile(file)
	}
}
