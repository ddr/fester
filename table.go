package main

import (
	"fmt"
	"strings"
)

type Row map[string]string

func (r *Row) HasKey(key string) bool {
	_, ok := (*r)[key]
	return ok
}

func (r *Row) HasValue(key string) bool {
	value, found := r.Get(key)
	return found && value != ""
}

func (r *Row) Get(key string) (value string, found bool) {
	value, found = (*r)[key]
	return value, found
}

func (r *Row) Set(key, value string) {
	(*r)[key] = value
}

func (r *Row) Model() string {
	model, _ := r.Get(ModelCol)
	return model
}

func (r *Row) SetModel(model string) {
	r.Set(ModelCol, model)
}

func (r *Row) Path() string {
	path, _ := r.Get(PathCol)
	return path
}

func (r *Row) SetPath(path string) {
	r.Set(PathCol, path)
}

func (r *Row) SetTitle(title string) {
	r.Set(TitleCol, title)
}

func (r *Row) LocalId() string {
	localId, _ := r.Get(LocalIdCol)
	return localId
}

func (r *Row) SetLocalId(localId string) {
	r.Set(LocalIdCol, localId)
}

func (r *Row) Title() string {
	title, _ := r.Get(TitleCol)
	return title
}

func (r *Row) NestedPath() string {
	nestedPath, _ := r.Get(NestedPathCol)
	return nestedPath
}

func (r *Row) SetNestedPath(nestedPath string) {
	// replace backslashes with forward slashes
	r.Set(NestedPathCol, strings.ReplaceAll(nestedPath, "\\", "/"))
}

func (r *Row) Checksum() string {
	checksum, _ := r.Get(ChecksumCol)
	return checksum
}

func (r *Row) SetChecksum(checksum string) {
	r.Set(ChecksumCol, checksum)
}

func (r *Row) Id() string {
	id, _ := r.Get(IdCol)
	return id
}

func (r *Row) HasId() bool {
	return r.HasValue(IdCol)
}

func (r *Row) HasPath() bool {
	return r.HasValue(PathCol)
}

func (r *Row) HasModel() bool {
	return r.HasValue(ModelCol)
}

func (r *Row) NormalizeModel() error {
	if !r.HasModel() {
		return fmt.Errorf("missing model: %s", r)
	}
	if IsValidModel(r.Model()) {
		return nil
	}
	normalizedModel := strings.ToUpper(r.Model()[:1]) + strings.ToLower(r.Model()[1:])
	if IsValidModel(normalizedModel) {
		r.SetModel(normalizedModel)
		return nil
	}
	return fmt.Errorf("invalid model: %s", r)
}

func (r *Row) HasChecksum() bool {
	return r.HasValue(ChecksumCol)
}

func (r *Row) HasTitle() bool {
	return r.HasValue(TitleCol)
}

func (r *Row) IsItem() bool {
	return r.Model() == ItemModel
}

func (r *Row) IsCollection() bool {
	return r.Model() == CollectionModel
}

func (r *Row) IsComponent() bool {
	return r.Model() == ComponentModel
}

func (r *Row) IsTarget() bool {
	return r.Model() == TargetModel
}

func (r *Row) IsAttachment() bool {
	return r.Model() == AttachmentModel
}

func (r *Row) IsContentResource() bool {
	return r.IsComponent() || r.IsTarget() || r.IsAttachment()
}

func (r *Row) IsFile() bool {
	return listContains(FileModels, r.Model())
}

func NewRow() *Row {
	return &Row{}
}

// A Table represents tablular data
type Table struct {
	Headers []string
	Rows    []Row
}

func (t *Table) HasHeader(header string) bool {
	return listContains(t.Headers, header)
}

func (t *Table) AddHeader(header string) {
	t.Headers = append(t.Headers, header)
}

func (t *Table) AddRow(row Row) int {
	t.Rows = append(t.Rows, row)
	return len(t.Rows) - 1
}

func (t *Table) Values(col string) []string {
	values := make([]string, len(t.Rows))
	for i, row := range t.Rows {
		if value, found := row.Get(col); found {
			values[i] = value
		}
	}
	return values
}

func (t *Table) Paths() []string {
	return t.Values(PathCol)
}

func (t *Table) Ids() []string {
	return t.Values(IdCol)
}

func (t *Table) Checksums() []string {
	return t.Values(ChecksumCol)
}

func (t *Table) IsDuplicateValue(col string, value string) bool {
	if value == "" {
		return false
	}
	for _, row := range t.Rows {
		if v, found := row.Get(col); found && v == value {
			return true
		}
	}
	return false
}
