package main

import (
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

var BlankLine = regexp.MustCompile(`^\s*$`)

func listIndex(list []string, value string) int {
	for i, v := range list {
		if v == value {
			return i
		}
	}
	return -1
}

func listContains(list []string, value string) bool {
	return listIndex(list, value) != -1
}

func skipBOM(file *os.File) (err error) {
	// check for byte order mark
	bom := make([]byte, len(ByteOrderMark))
	_, err = file.Read(bom)
	if err != nil && err != io.EOF {
		return err
	}
	if string(bom) != ByteOrderMark {
		// no BOM, rewind file
		_, err = file.Seek(0, 0)
		if err != nil {
			return err
		}
	}
	return nil
}

func normalizeHeaders(headers []string) ([]string, error) {
	for i, header := range headers {
		if header == "" {
			return headers, ErrHeaderEmpty
		}
		headers[i] = strings.ToLower(strings.TrimSpace(header))
	}
	return headers, nil
}

func normalizeRow(row []string) []string {
	for i, cell := range row {
		row[i] = strings.TrimSpace(cell)
	}
	return row
}

func isBlankOrCommentLine(line string) bool {
	return strings.HasPrefix(line, "#") || BlankLine.MatchString(line)
}

// Returns the base path with the extension removed
func filePrefix(path string) string {
	return strings.TrimSuffix(filepath.Base(path), filepath.Ext(path))
}
