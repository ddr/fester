package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestOptionsSetRootAbs(t *testing.T) {
	o := Options{}
	o.Root.Set("/path/to/root")
	if o.Root.String() != "/path/to/root" {
		t.Errorf("Expected '/path/to/root', got '%s'", o.Root)
	}
}

func TestOptionsSetRootRel(t *testing.T) {
	o := Options{}
	o.Root.Set("test")
	root, _ := filepath.Abs("test")
	if o.Root.String() != root {
		t.Errorf("Expected '%s', got '%s'", root, o.Root)
	}
}

func TestOptionsSetRootDot(t *testing.T) {
	o := Options{}
	o.Root.Set(".")
	root, _ := filepath.Abs(".")
	if o.Root.String() != root {
		t.Errorf("Expected '%s', got '%s'", root, o.Root)
	}
}

func TestOptionsSetRootEmpty(t *testing.T) {
	o := Options{}
	o.Root.Set("")
	root, _ := filepath.Abs("")
	if o.Root.String() != root {
		t.Errorf("Expected '%s', got '%s'", root, o.Root)
	}
}

func TestOptionsSetRootNil(t *testing.T) {
	o := Options{}
	o.Root.Set("")
	root, _ := filepath.Abs("")
	if o.Root.String() != root {
		t.Errorf("Expected '%s', got '%s'", root, o.Root)
	}
}

func TestOptionsLoadDefaultPath(t *testing.T) {
	wd, _ := os.Getwd()
	os.Chdir("test/options")
	defer os.Chdir(wd)
	o := Options{}
	o.LoadDefaultPath()
	if o.CaptionFiles != "captions" {
		t.Errorf("Expected 'captions', got '%s'", o.CaptionFiles)
	}
	if o.ChecksumFile != "manifest-sha1.txt" {
		t.Errorf("Expected 'manifest-sha1.txt', got '%s'", o.ChecksumFile)
	}
	if o.ContentFiles != "content" {
		t.Errorf("Expected 'content', got '%s'", o.ContentFiles)
	}
	if o.DerivedImageFiles != "derivs" {
		t.Errorf("Expected 'derivs', got '%s'", o.DerivedImageFiles)
	}
	if len(o.ExcludeContentPaths) != 1 {
		t.Errorf("Expected length 1, got '%d'", len(o.ExcludeContentPaths))
	}
	if len(o.ExcludePathNames) != 2 {
		t.Errorf("Expected length 1, got '%d'", len(o.ExcludePathNames))
	}
	if o.IntermediateFiles != "intermediates" {
		t.Errorf("Expected 'intermediates', got '%s'", o.IntermediateFiles)
	}
	if o.Mailto[0] != "no-reply@duke.edu" {
		t.Errorf("Expected ['no-reply@duke.edu'], got '%s'", o.Mailto)
	}
	if o.MetadataFile != "metadata.txt" {
		t.Errorf("Expected 'metadata.txt', got '%s'", o.MetadataFile)
	}
	if o.MultiresImageFiles != "multires" {
		t.Errorf("Expected 'multires', got '%s'", o.MultiresImageFiles)
	}
	if o.PackageType != "standard" {
		t.Errorf("Expected 'standard', got '%s'", o.PackageType)
	}
	if o.Root != "." {
		t.Errorf("Expected '.', got '%s'", o.Root)
	}
	if o.StreamableMediaFiles != "streamable" {
		t.Errorf("Expected 'streamable', got '%s'", o.StreamableMediaFiles)
	}
	if o.TargetFiles != "targets" {
		t.Errorf("Expected 'targets', got '%s'", o.TargetFiles)
	}
	if o.ThumbnailFiles != "thumbnails" {
		t.Errorf("Expected 'thumbnails', got '%s'", o.ThumbnailFiles)
	}
}

func TestOptionsLoadPath(t *testing.T) {
	o := Options{}
	o.LoadPath("test/options/custom.json")
	if o.CaptionFiles != "captions" {
		t.Errorf("Expected 'captions', got '%s'", o.CaptionFiles)
	}
	if o.ChecksumFile != "manifest-sha1.txt" {
		t.Errorf("Expected 'manifest-sha1.txt', got '%s'", o.ChecksumFile)
	}
	if o.ContentFiles != "content" {
		t.Errorf("Expected 'content', got '%s'", o.ContentFiles)
	}
	if o.DerivedImageFiles != "derivs" {
		t.Errorf("Expected 'derivs', got '%s'", o.DerivedImageFiles)
	}
	if len(o.ExcludeContentPaths) != 1 {
		t.Errorf("Expected length 1, got '%d'", len(o.ExcludeContentPaths))
	}
	if len(o.ExcludePathNames) != 2 {
		t.Errorf("Expected length 1, got '%d'", len(o.ExcludePathNames))
	}
	if o.IntermediateFiles != "intermediates" {
		t.Errorf("Expected 'intermediates', got '%s'", o.IntermediateFiles)
	}
	if o.Mailto[0] != "no-reply@duke.edu" {
		t.Errorf("Expected ['no-reply@duke.edu'], got '%s'", o.Mailto)
	}
	if o.MetadataFile != "metadata.txt" {
		t.Errorf("Expected 'metadata.txt', got '%s'", o.MetadataFile)
	}
	if o.MultiresImageFiles != "multires" {
		t.Errorf("Expected 'multires', got '%s'", o.MultiresImageFiles)
	}
	if o.PackageType != "standard" {
		t.Errorf("Expected 'standard', got '%s'", o.PackageType)
	}
	if o.Root != "." {
		t.Errorf("Expected '.', got '%s'", o.Root)
	}
	if o.StreamableMediaFiles != "streamable" {
		t.Errorf("Expected 'streamable', got '%s'", o.StreamableMediaFiles)
	}
	if o.TargetFiles != "targets" {
		t.Errorf("Expected 'targets', got '%s'", o.TargetFiles)
	}
	if o.ThumbnailFiles != "thumbnails" {
		t.Errorf("Expected 'thumbnails', got '%s'", o.ThumbnailFiles)
	}
}

// test setting ManifestFile and not setting PackageType
func TestOptionsValidateManifestFile(t *testing.T) {
	o := Options{}
	o.ManifestFile.Set("test/manifest/manifest_valid.csv")
	err := o.Validate()
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
}
