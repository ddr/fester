package main

import (
	"errors"
	"log"
	"os"
	"testing"
)

// TestFesterError tests Fester.Error()
func TestFesterError(t *testing.T) {
	f := NewFester()
	f.Error(errors.New("test error"))
	if len(f.Messages) != 1 {
		t.Errorf("Expected 1 error, got %d", len(f.Messages))
	}
	if f.Messages[0].Text != "test error" {
		t.Errorf("Expected 'test error', got '%v'", f.Messages[0])
	}
}

// TestFesterChecksum tests Fester.Checksum()
func TestFesterChecksum(t *testing.T) {
	f := NewFester()
	f.Checksums.Add("/path/to/file", "1234567890")
	checksum, _ := f.GetChecksum("/path/to/file")
	if checksum != "1234567890" {
		t.Errorf("Expected '1234567890', got '%s'", checksum)
	}
}

// TestFesterChecksumMissing tests Fester.Checksum() with a missing checksum
func TestFesterChecksumMissing(t *testing.T) {
	f := NewFester()
	checksum, _ := f.GetChecksum("/path/to/file")
	if checksum != "" {
		t.Errorf("Expected '', got '%s'", checksum)
	}
}

// TestFesterChecksumMissingError tests Fester.Checksum() with a missing checksum
func TestFesterChecksumMissingError(t *testing.T) {
	f := NewFester()
	f.ChecksumFile.Set("test/checksums/checksums_valid.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.ParseChecksumFile()
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if checksum, found := f.GetChecksum("/path/to/file"); found {
		t.Errorf("Expected '', got '%s'", checksum)
	}
}

// TestFesterRootVolumePath tests Fester.RootVolumePath()
func TestFesterContentVolumePath(t *testing.T) {
	f := NewFester()
	f.Root.Set("/nas/dpc_archive/2016")
	f.ContentVolume = DPCArchiveVolume

	// Test path not on content volume
	contentVolumePath, err := f.ContentVolumePath("/path/to/root")
	if err == nil {
		t.Errorf("Expected error, got nil")
	}
	if contentVolumePath != "/path/to/root" {
		t.Errorf("Expected '/path/to/root', got '%s'", contentVolumePath)
	}
	if len(f.Messages) != 0 {
		t.Errorf("Expected 0 messages, got %d", len(f.Messages))
	}
	// Test server path
	contentVolumePath, err = f.ContentVolumePath("/nas/dpc_archive/2016/2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
	// Test Mac path
	contentVolumePath, err = f.ContentVolumePath("/Volumes/DPC-Archive/2016/2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
	// Test UNC path - forward slashes
	contentVolumePath, err = f.ContentVolumePath("//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
	// Test UNC path - backslashes
	contentVolumePath, err = f.ContentVolumePath("\\\\oit-nas-fe11.oit.duke.edu\\DPC-Archive\\2016\\2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
	// Test UNC path - mixed slashes
	contentVolumePath, err = f.ContentVolumePath("\\\\oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
	// Test UNC path - different case
	contentVolumePath, err = f.ContentVolumePath("\\\\oit-nas-fe11.oit.duke.edu/Dpc-archive/2016/2016-01-01")
	if err != nil {
		t.Errorf("Expected no error, got '%s'", err)
	}
	if contentVolumePath != "//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016/2016-01-01', got '%s'", contentVolumePath)
	}
}

func TestFesterContentVolume(t *testing.T) {
	f := NewFester()
	f.Root.Set("/nas/dpc_archive/2016")
	f.ContentVolume = DPCArchiveVolume
	if f.ContentVolume != "//oit-nas-fe11.oit.duke.edu/DPC-Archive" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive', got '%s'", f.ContentVolume)
	}

	f.Root.Set("/Volumes/DPC-Archive/2016")
	if f.ContentVolume != "//oit-nas-fe11.oit.duke.edu/DPC-Archive" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive', got '%s'", f.ContentVolume)
	}

	// Can't use Options.Root.Set() on non-Windows with UNC paths
	f.Root = AbsPath("//oit-nas-fe11.oit.duke.edu/DPC-Archive/2016")
	if f.ContentVolume != "//oit-nas-fe11.oit.duke.edu/DPC-Archive" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive', got '%s'", f.ContentVolume)
	}

	f.Root = AbsPath("\\\\oit-nas-fe11.oit.duke.edu\\DPC-Archive\\2016")
	if f.ContentVolume != "//oit-nas-fe11.oit.duke.edu/DPC-Archive" {
		t.Errorf("Expected '//oit-nas-fe11.oit.duke.edu/DPC-Archive', got '%s'", f.ContentVolume)
	}
}

// TestFesterGenerateManifest tests Fester.GenerateManifest()
func TestFesterGenerateManifestStandardPackage(t *testing.T) {
	f := NewFester()
	f.PackageType.Set("standard")
	f.Root.Set("test/manifest/standard")
	f.ContentFiles.Set("test/manifest/standard/data")
	f.MetadataFile.Set("test/manifest/standard/metadata.txt")
	f.ChecksumFile.Set("test/manifest/standard/manifest-sha1.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.GenerateManifest()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.Messages) != 0 {
		for _, message := range f.Messages {
			if message.IsError() {
				t.Errorf("Error: %v", message)
			}
		}
	}
	if !f.Manifest.HasHeader(ModelCol) {
		t.Errorf("Expected manifest to have header '%s'", ModelCol)
	}
	if !f.Manifest.HasHeader(PathCol) {
		t.Errorf("Expected manifest to have header '%s'", PathCol)
	}
	if f.ManifestItemCount() != 2 {
		t.Errorf("Expected 2 items, got %d", f.ManifestItemCount())
	}
	if f.ManifestComponentCount() != 2 {
		t.Errorf("Expected 2 components, got %d", f.ManifestComponentCount())
	}
	for _, row := range f.Manifest.Rows {
		if row.IsComponent() && row.Checksum() == "" {
			t.Errorf("Expected checksum for path '%s', got none", row.Path())
		}
	}
}

// TestFesterGenerateManifestGenericPackage tests Fester.GenerateManifest()
func TestFesterGenerateManifestGenericPackage(t *testing.T) {
	f := NewFester()
	f.PackageType = "generic"
	f.Root.Set("test/manifest/generic")
	f.ChecksumFile.Set("test/manifest/generic/manifest-sha1.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.GenerateManifest()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if len(f.Messages) != 0 {
		for _, msg := range f.Messages {
			if msg.IsError() {
				t.Errorf("Error: %v", msg)
			}
		}
	}
	if !f.Manifest.HasHeader(ModelCol) {
		t.Errorf("Expected manifest to have header '%s'", ModelCol)
	}
	if !f.Manifest.HasHeader(PathCol) {
		t.Errorf("Expected manifest to have header '%s'", PathCol)
	}
	if f.ManifestItemCount() > 0 {
		t.Errorf("Expected 0 items, got %d", f.ManifestItemCount())
	}
	if f.ManifestComponentCount() != 2 {
		t.Errorf("Expected 2 components, got %d", f.ManifestComponentCount())
	}
}

// TestFesterGenerateManifestNestedPackage tests Fester.GenerateManifest()
func TestFesterGenerateManifestNestedPackage(t *testing.T) {
	f := NewFester()
	f.PackageType = "nested"
	//f.Debug = true
	f.Root.Set("test/manifest/nested")
	f.ContentFiles.Set("test/manifest/nested/content")
	f.MetadataFile.Set("test/manifest/nested/metadata.txt")
	f.ChecksumFile.Set("test/manifest/nested/checksums.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.GenerateManifest()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	log.Printf("Metadata: %v\n", f.Metadata)
	log.Printf("metadata index: %v\n", f.metadataIndex)
	if len(f.Messages) != 0 {
		for _, err := range f.Messages {
			t.Errorf("Error: %v", err)
		}
	}
	if f.ManifestItemCount() != 2 {
		t.Errorf("Expected 2 items, got %d", f.ManifestItemCount())
	}
	if !f.Manifest.HasHeader(ModelCol) {
		t.Errorf("Expected manifest to have header '%s'", ModelCol)
	}
	// manfiest headers should include nested_path
	if !f.Manifest.HasHeader(NestedPathCol) {
		t.Errorf("Expected manifest to have header '%s'", NestedPathCol)
	}
	// metadata should be present
	if !f.Manifest.HasHeader(TitleCol) {
		t.Errorf("Expected manifest to have header '%s'", TitleCol)
	}
	if f.ManifestRows()[0].Title() != "Picture 1" {
		t.Errorf("Expected title of first Item to be 'Picture 1', got '%s'", f.ManifestRows()[0].Title())
	}
	if len(f.ManifestRows()) > 2 && f.ManifestRows()[2].Title() != "Picture 2" {
		t.Errorf("Expected title of second Item to be 'Picture 2', got '%s'", f.ManifestRows()[2].Title())
		t.Errorf("Manifest rows: %v", f.ManifestRows())
		t.Errorf("Manifest: %v", f.Manifest)
	}
	// manifest rows should include nested_path
	for _, row := range f.ManifestRows() {
		if row.IsItem() && row.NestedPath() == "" {
			t.Errorf("Expected nested_path for Item at path '%s', got none", row.Path())
		}
	}
	if f.ManifestComponentCount() != 2 {
		t.Errorf("Expected 2 components, got %d", f.ManifestComponentCount())
	}
}

// TestFesterGenerateManifestDPCPackage tests Fester.GenerateManifest()
func TestFesterGenerateManifestDPCPackage(t *testing.T) {
	f := NewFester()
	f.PackageType = "dpc"
	f.ItemIdLength = 11
	f.MetadataFile.Set("test/manifest/dpc/metadata.csv")
	f.Root.Set("test/manifest/dpc")
	f.ContentFiles.Set("test/manifest/dpc/content")
	f.ChecksumFile.Set("test/manifest/dpc/dpc-checksums-sha1.txt")
	wd, _ := os.Getwd()
	f.ContentVolume = wd
	err := f.GenerateManifest()
	if err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if !f.Manifest.HasHeader(ModelCol) {
		t.Errorf("Expected manifest to have header '%s'", ModelCol)
	}
	if !f.Manifest.HasHeader(LocalIdCol) {
		t.Errorf("Expected manifest to have header '%s'", LocalIdCol)
	}
	if f.ManifestItemCount() != 1 {
		t.Errorf("Expected 1 Item model, got %d", f.ManifestItemCount())
	}
	if f.ManifestComponentCount() != 2 {
		t.Errorf("Expected 2 Component models, got %d", f.ManifestComponentCount())
	}
}

func TestFesterMergeOptions(t *testing.T) {
	f := NewFester()
	f.ChecksumFile.Set("test/checksums/checksums_invalid.txt")
	f.NonInteractive = true
	o := Options{
		ChecksumFile:   "test/checksums/checksums_valid.txt",
		NonInteractive: false,
		PackageType:    "standard",
	}
	f.MergeOptions(&o)

	// non-zero value should not override non-zero value
	if f.ChecksumFile != "test/checksums/checksums_valid.txt" {
		t.Errorf("Expected 'test/checksums/checksums_valid.txt', got '%s'", f.ChecksumFile)
	}
	// zero value should not override non-zero value
	if f.NonInteractive != true {
		t.Errorf("Expected 'true', got '%v'", f.NonInteractive)
	}
	// non-zero value should override zero value
	if f.PackageType != StandardPackage {
		t.Errorf("Expected '%s', got '%s'", StandardPackage, f.PackageType)
	}
}

// TestFesterInteractive tests Fester.Interactive()
func TestFesterInteractive(t *testing.T) {
	f := NewFester()
	if !f.Interactive() {
		t.Errorf("Expected true, got false")
	}

	f.NonInteractive = true
	if f.Interactive() {
		t.Errorf("Expected false, got true")
	}
}
