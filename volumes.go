package main

import (
	"path/filepath"
)

const (
	DDRTestDataVolume       = "//oit-nas-fe10.oit.duke.edu/ddr-test-data"
	DPCArchiveVolume        = "//oit-nas-fe11.oit.duke.edu/DPC-Archive"
	DPCMSIVolume            = "//oit-nas-fe11.oit.duke.edu/dpc_msi"
	DPCStagingVolume        = "//cx4-fe-nas01.oit.duke.edu/TUCASI_CIFS1/DPC-Work/staging"
	LibStagingVolume        = "//oit-nas-fe11.oit.duke.edu/lib_staging"
	RubensteinArchiveVolume = "//oit-nas-fe11.oit.duke.edu/RL-Archive"
)

// Known storage volumes
var StorageVolumes = []string{
	DDRTestDataVolume, DPCArchiveVolume, DPCMSIVolume,
	DPCStagingVolume, LibStagingVolume, RubensteinArchiveVolume,
}

var DDRMounts = map[string]string{
	DDRTestDataVolume:       "/nas/ddr_test_data",
	DPCArchiveVolume:        "/nas/dpc_archive",
	DPCMSIVolume:            "/nas/dpc_msi",
	DPCStagingVolume:        "/nas/dpc_staging",
	LibStagingVolume:        "/nas/lib_staging",
	RubensteinArchiveVolume: "/nas/rubenstein_archive",
}

func DDRMount(volume string) string {
	return DDRMounts[volume]
}

func MacMount(volume string) string {
	if volume == DPCStagingVolume { // special case
		return "/Volumes/TUCASI_CIFS1/DPC-Work/staging"
	}
	return "/Volumes/" + filepath.Base(volume)
}
