package main

import (
	"testing"
)

// TestFesterParseManifestFileMissing tests Fester.ParseManifestFile() with a missing manifest file
func TestFesterParseManifestFileMissing(t *testing.T) {
	f := NewFester()
	f.ManifestFile.Set("test/missing.txt")
	err := f.ParseManifestFile()
	if err == nil {
		t.Errorf("Expected error, got none")
	}
}

// TestFesterParseManifestFileInvalid tests Fester.ParseManifestFile() with an invalid manifest file
func TestFesterParseManifestFileInvalid(t *testing.T) {
	f := NewFester()
	f.ManifestFile.Set("test/manifest/manifest_invalid.csv")
	err := f.ParseManifestFile()
	if err != ErrHeaderEmpty {
		t.Errorf("Expected empty header error, got %v", err)
	}
}

// TestFesterParseManifestFile tests Fester.ParseManifestFile()
func TestFesterParseManifestFileValid(t *testing.T) {
	f := NewFester()
	f.ManifestFile.Set("test/manifest/manifest_valid.csv")
	err := f.ParseManifestFile()
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if f.ManifestCollectionCount() != 1 {
		t.Errorf("Expected 1 collection, got %d", f.ManifestCollectionCount())
	}
	if f.ManifestItemCount() != 1 {
		t.Errorf("Expected 1 item, got %d", f.ManifestItemCount())
	}
	if f.ManifestComponentCount() != 1 {
		t.Errorf("Expected 1 component, got %d", f.ManifestComponentCount())
	}
}

func TestFesterParseManifestFileWIthFileIds(t *testing.T) {
	f := NewFester()
	f.ManifestFile.Set("test/manifest/manifest_with_file_ids.csv")
	err := f.ParseManifestFile()
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if f.ManifestComponentCount() != 3 {
		t.Errorf("Expected 3 components, got %d", f.ManifestComponentCount())
	}
}

// test parsing manifest file with extra whitespace
func TestFesterParseManifestFileWithExtraWhitespace(t *testing.T) {
	f := NewFester()
	f.ManifestFile.Set("test/manifest/manifest_valid_extra_whitespace.csv")
	err := f.ParseManifestFile()
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}
	if f.ManifestCollectionCount() != 1 {
		t.Errorf("Expected 1 collection, got %d", f.ManifestCollectionCount())
	}
	if f.ManifestItemCount() != 1 {
		t.Errorf("Expected 1 item, got %d", f.ManifestItemCount())
	}
	if f.ManifestComponentCount() != 1 {
		t.Errorf("Expected 1 component, got %d", f.ManifestComponentCount())
	}
	if !listContains(f.ManifestHeaders(), "title") {
		t.Errorf("Expected title header, got none")
	}
}
