# Fester - A tool for generating DDR ingest manifests

- [Download](#download)
- [Summary](#summary)
- [Important Path Notes](#important-path-notes)
- [Building From Source](#building-from-source)
- [Generating and Using a Configuration File](#generating-and-using-a-configuration-file)
- [Generating a Manifest from Source Files](#generating-a-manifest-from-source-files)
- [Handling Multiple Collections](#handling-multiple-collections)
- [Using an Existing Manifest File](#using-an-existing-manifest-file)
- [Using a Checksum File](#using-a-checksum-file)
- [Using a Metadata File](#using-a-metadata-file)
- [Ingest Manifest Specification](#ingest-manifest-specification)

## Download

Pre-compiled binaries for Windows, Mac (amd64/Intel and arm64/M1), and Linux are available from the [package registry](https://gitlab.oit.duke.edu/ddr/fester/-/packages).

## Summary

Mac/Linux: 

    $ fester -package-type [dpc|generic|nested|standard] [options] <root path>
    $ fester -generate-config [options] <root path>
    $ fester -manifest-file <path> [options]
    $ fester -help
    $ fester -version

Windows:

    > fester.exe -package-type [dpc|generic|nested|standard] [options] <root path>
    > fester.exe -generate-config [options] <root path>
    > fester.exe -manifest-file <path> [option]
    > fester.exe -help
    > fester.exe -version

## Important Path Notes

- When connecting to network volumes, create the network location to the top-level volume directory, not a subdirectory.  This will ensure that paths will match known volume locations.

Windows example:

    USE: \\cx4-fe-nas01.oit.duke.edu\TUCASI_CIFS1
    NOT: \\cx4-fe-nas01.oit.duke.edu\TUCASI_CIFS1\DPC-Work

Mac example:

    USE: smb://cx4-fe-nas01.oit.duke.edu/TUCASI_CIFS1
    NOT: smb://cx4-fe-nas01.oit.duke.edu/TUCASI_CIFS1/DPC-Work

- On Windows, you *may* use mapped drive letters for paths in `fester` command options.  Using  
drive letters for paths in metadata or checksum files, however, is not supported because we cannot easily and reliably transform a drive letter to a fully-qualified UNC path.

Example (OK):

    > C:\Users\me\fester.exe -package-type nested -metadata-file D:\Network\Drive\Folder\metadata.txt

- If `fester` is having trouble with paths in a particular case, try using the `-debug` option to get detailed output of the path handling.  This output could be useful to developers in troubleshooting.

## Building From Source

If you're looking for a pre-built executable, please visit the [Package Registry](https://gitlab.oit.duke.edu/ddr/fester/-/packages) and use the latest release for your platform.  Building from source is only necessary to use unreleased code.

Clone the repo

    $ git clone https://gitlab.oit.duke.edu/ddr/fester

Build the module (requires Go 1.19+)

    $ cd fester
    $ git pull origin main  # if you didn't just clone
    $ go build

Creates `fester` or `fester.exe` in the working directory. Use `go build -o /path/to/fester` to build to alternate location.

## Generating and Using a Configuration File

*As of fester v1.0.0, this feature has not been tested in the field.  Please report any bugs or unexpected behavior when using a configuration file.*

`fester` supports a JSON config file of options in the following order of precedence:

- Via STDIN -- i.e., I/O redirection or pipe
- Via the `-config-file` option
- A file name `.fester.json` in the working directory

See the example file in this package.

You can generate a config file with the `fester -generate-config` command.
`fester` prints a config file STDOUT using the default settings
and any other options given on the command line.

    $ fester -generate-config

Example output (Windows):

```json
{
    "Root": "C:\\Users\\dc",
    "CaptionFiles": "",
    "ChecksumFile": "",
    "ContentFiles": "",
    "Debug": false,
    "DerivedImageFiles": "",
    "Mailto": "",
    "ExcludePathNames": [
        ".*",
        "Thumbs.db"
    ],
    "ExcludeContentPaths": [
        "C:\\Users\\dc\\.fester.json"
    ],
    "ExtractedTextFiles": "",
    "IntermediateFiles": "",
    "ItemIdLength": 0,
    "ManifestFile": "",
    "MetadataFile": "",
    "MultiresImageFiles": "",
    "NonInteractive": false,
    "OutputFile": "",
    "PackageType": "",
    "StreamableMediaFiles": "",
    "TargetFiles": "",
    "ThumbnailFiles": "",
    "Verbose": false
}
```

## Generating a Manifest from Source Files

Generating a manifest requires the specification, in the configuration file or with the `-package-type` option, of a valid package type. A minimal example to generate a manifest without a config file would be:

    $ fester -package-type dpc

You can specify an alternate root folder with the last argument to the command:

    Mac/Linux $ fester -package-type dpc //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOO

    WIN/> fester.exe -package-type dpc \\oit-nas-fe11.oit.duke.edu\DPC-Archive\na_FOO

Relative paths for options that refer to directories or files are considered relative to the *working directory*, not the root (if different from the working directory).  This includes using `..` for a parent directory, so, for example, `-checksum-file ../manifest-sha1.txt` would mean `manifest-sha1.txt` in the parent directory of the working directory.

Manifest data by default is printed to `STDOUT`; informational messages are printed to `STDERR`.
If you want to print the manifest to a file, you can use the `-output-file` option:

    $ fester -package-type standard -output-file manifest.csv 

`<root path>` is optional, set to the working directory by default, and is also the default location for content files, unless the `-content-files` option is used.

## Handling Multiple Collections

*Added in v1.2.0-beta.2 - for DPC Folder package type only.*

Fester can generate a manifest for multiple collections using the `-collection-per-folder` option, with the following conditions:

- Each collection must be a top-level subfolder of the content files folder (root path or `-content-files` path). A collection folder may contain files and subfolders as appropriate.
- Other options, such as `-checksum-file`, `-metadata-file`, `-target-files`, etc., must apply across all collections; i.e., each collection uses the same options.
- Since a collection must have either a title or ID, `fester` will set a default title value in the manifest if there is no matching metadata for the collection.
- Target files, if present, will be added to the end of the manifest (as usual) and will have to be manually repositioned by the curator to be associated with the correct collections.  In a future release, `fester` may be able to collate target files with collections, depending on whether changes can be made to target naming conventions.

## Using an Existing Manifest File

Use the `-manifest-file` option to provide an existing manifest file as input in the generation of a final manifest.

If this option is used *without* `-package-type`, `fester` assumes that there are no content files to be considereBd in generating the final manifest.

The source manifest is parsed, normalized, and validated for correctness.  It also permits adding rows for non-original files without parent Component rows, provided that there is an `id` column and each file row contains the id of its Component. In that case, `fester` will pluck the ids out of the file rows and generate the appropriate Component rows for you.

## Using a Checksum File

Use the `-checksum-file` option to specify a file to source for content file checksums. 

- The file is expected to have one line for each file, where the line consists of a SHA1 hex digest and a file path, in that order, separated by whitespace. 
- The file must consist of valid UTF-8 data according to the [Go encoding/utf8 standard library](https://pkg.go.dev/unicode/utf8#ValidString) (added in v1.1.1).
- File paths may contain spaces, but not lines breaks (`\r` and/or `\n`) or other control characters. Legitimate file paths that cannot meet these criteria should be handled manually in the manifest.
- Relative paths in the checksum file will be converted to absolute paths based on the *directory containing the checksum file*.
- Blank lines and comments (lines starting with `#`) are ignored.
- The specified file will be used as the checksum source for all content files found by the manifest generator. Multiple valid checksum files should be concatened into a single file prior to manifest generation.
- Missing checksums will be reported for Components; other files may optionally have checksums, but they are not required (by `fester`).
- The standard ingest package by default looks for `manifest-sha1.txt` in the root of the package (this is the SHA1 checksum file in the BagIt spec), but the `-checksum-file` option will override the default.
- When a checksum is matched with a Component or other file, the SHA1 digest is inserted into the manifest row in the `sha1` column.

## Using a Metadata File

Use the `-metadata-file` option to specify a CSV file containing metadata fields for the ingest.

*NOTE: For legacy reasons, the parser will accept a tab-separated (TSV) if that format is detected, but this behavior is deprecated and may be removed from a future release.*

- Leading white space is trimmed from values
- Blank lines and comments (lines starting with `#`) are ignored.
- The `standard` package by default looks for a metadata file named `metadata.txt` in the root path, 
but the `-metadata-file` option will override.

### The path column

For `standard` and `nested` package types, the metadata file has one content requirement, the `path` column:

- Values must be either absolute file/folder paths, or paths relative to the content files directory (as specified by the `-content-files` option or root path).
- Rows with empty paths will be skipped and reported as errors but will not cause the manifest generation to fail. 
- A duplicate path within a metadata file is a fatal error.

Note: `fester` will attempt to resolve absolute paths that refer to known storage volumes as mounted on DDR servers (prefixed with `/nas/`) or on a Mac workstation (under `/Volumes/`).  On Windows, a full path must be given as a UNC path.  We cannot resolve paths beginning with drive letters.

Metadata fields for a content file matching the path will be merged into the manifest in the column order given in the metadata file (see Ordering section below for details).

Note that the manifest prints "Unix-style" (forward-slash separated) paths rooted in the storage volume for content objects (Components, etc.) and non-content files, since these are used to ingest the actual files.  Paths for Items, if present, are made relative to the root/content files directory (the main purpose for an Item path would be to set the `nested_path` attribute for a nested path ingest).

## Ingest Manifest Specification

### Row Order

The ordering of rows is used to indicate parent/child relationships between Items and Components, and File associations with Components. Target and Attachment resources may appear in any order; it is recommended that they appear either at the end or beginning of the manifest -- i.e., not interleaved with Items.  The ordering of the Item rows (with respect to each other) is not considered significant.  The same is true for Component rows associated with a particular Item: the order is not significant (we use other criteria, such as `local_id`, for ordering within the DDR).

Components associated with (i.e., children of) a particular Item must appear immediately after/below the parent Item and before/above subsequent Items.

The original file (a.k.a. "content") for a Component is represented in the Component row itself.  Other files, such as `streamable_media`, `thumbnail`, etc., associated with a Component must appear immediately after/below the Component row and before/above other non-file rows.  See the `model` column documentation below.

#### Example 

| model  | local_id    | path    | 
| ------ | ----------- | ------- | 
| Item   | foobar01001 |         | 
| Component | foobar0100101 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100101.tif |
| File.thumbnail | | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/thumbnails/foobar0100101.png |
| Component | foobar0100102 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100102.tif |
| File.thumbnail | | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/thumbnails/foobar0100102.png |
| Item | foobar01002 | |
| Component | foobar0100201 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100201.tif |
| Component | foobar0100202 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100202.tif |
| Component | foobar0100203 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100203.tif |
| Item | foobar01003 | |
| Component | foobar0100301 | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/foobar0100301.tif |
| Target  | | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/targets/target01.tif |
| Target  | | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/targets/target02.tif |
| Target  | | //oit-nas-fe11.oit.duke.edu/DPC-Archive/na_FOOBAR/targets/target03.tif |

### Manifest Columns

- Columns that have special significance in the manifest - `id`, `model`, `path`, and `sha1` are discussed in detail below.  
- Other columns must have valid metadata DDR field names (headers are normalized to lowercase and stripped of whitespace).
- `fester` does not currently implement a field validation routine, but the manifest ingest process does validate fields.  
- Some valid fields are not allowed to be set by ingest or other batch processes.  Consult the [metadata documentation](https://duldev.atlassian.net/wiki/spaces/DDRDOC/pages/30020277/Metadata+Documentation) for more information.
- Columns may appear in any order.
- Columns may *not* be repeated.
- A pipe `|` character within a column is interpreted as a delimiter of multiple values for that attribute.

#### id

The ID of the resource (UUID), when used for an update operation. `fester` does not generate IDs for resources to be ingested.

#### model

The `model` column is for the "unqualified" (omitting `Ddr::` prefix) model type of the resource - `Item`, `Component`, `Target`, etc.  

- Model specification is *required for the creation of new resources*.
- `fester` generates the `model` column value for Item and Component resources based on the `-package-type` and other options, as described below:

`dpc` DPC folder package

  - Files in the content folder tree (root path) or `-content-files` path) are interpreted as Components -- except for files in `-target-files` or `*-files` locations that may be nested in the content folder.
  - Content subfolders (not for targets or other files) are not meaningful per se.
  - Items are determined according to a well-known formula based on the value of `-item-id-length`.
  - The default (or item ID length 0) is to generate one parent Item for every Component.

`standard` Standard package (BagIt)

  - In the `data` directory of the BagIt bag (or the directory given in the `-content-files` option), each folder represents an Item and the files contained in the folder represent the child Components of that Item.

`nested` Nested folder package

  - Each file found in the content folder tree generates one Item and one child Component.

`generic` Generic folder package

  - No Items are created, only Components and other files
  - One Component is added for each content file

`-target-files PATH`

  - All `*.tif` files found under that directory path will generate Target rows.

Other file options:

```
-caption-files PATH
-derived-image-files PATH
-extracted-text-files PATH
-intermediate-files PATH
-multires-image-files PATH
-streamable-media-files PATH
-thumbnail-files PATH
```

  - Files under paths given with any of the options above are interpreted as being of the respective type, and are associated with a Component file having a matching base file name, extension excluded.  For example, an intermediate file named `recording.mp3` would be matched with a Component file `recording.wav`.
  - The `model` column for a file of this kind will adhere to the pattern `File.[FILE_ID]` where `FILE_ID` corresponds to the name that is used to identify the function of the file:
    - File.caption
    - File.derived_image
    - File.extracted_text
    - File.fits_file
    - File.intermediate_file
    - File.multires_image
    - File.streamable_media
    - File.struct_metadata
    - File.thumbnail

*Note: `File.fits_file` and `File.struct_metadata` are included for completeness, although `fester` does not provide options for adding these files automatically in manifest generation.*

#### path

The `path` column is interpreted according to the value in the `model` column:

- `Collection` - N/A
- `Item` - The `nested_path` attribute for the Item (nested folder ingest only).
- `Component` - Path to the original file ("content") of the Component.
- `File.[FILE_ID]` - Path to the `[FILE_ID]` file (`streamable_media`, `thumbnail`, etc.) associated with
  the nearest Component above in the manifest.
- `Target` - Path to the original file ("content") of the Target.
- `Attachment` - Path to the original file ("content") of the Attachment.

#### sha1

The `sha1` column is for recording SHA1 digests for files.  These digests may be merged in from an external file (as described above) or manually entered.
